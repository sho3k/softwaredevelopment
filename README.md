# SoftwareDevelopment

Project description:
- Simulate a traffic intersection existing of a simulator and a controller.
- The simulator should be compatable with all contestant controllers
- The controller should be compatable with all contestant simulators

Controller:
- The controller should control the state of all traffic lights
- Has a minimalistic user interface to show whats going on

Simulator:
- Simulates traffic on the controller and displays the controller output in the simulated space


# Research

| Behaviour | Meaning 
| --- | --- 
| Red | Stop 
| Amber | Stop; if driver is too close -> pursue 
| Amber Blinking | Out of order
| Green | Pursue

`Amber is usually shown around 3 seconds before turning to red`


43. Tweekleurige verkeerslichten mogen worden toegepast in situaties waarin slechts bepaalde verkeersstromen moeten worden gestopt en wel
    - op kruisingen en splitsingen van wegen nabij beveiligde overwegen en beweegbare bruggen. Alleen die richtingen, die de ontruiming van de overweg of de brug in de weg staan mogen worden voorzien van tweekleurige verkeerslichten;
    - in geval van automatische detectie van verkeersovertredingen;
    - in geval van automatische hoogtedetectie;
    - bij uitritten van hulpverleningsdiensten;
    - bij beweegbare bruggen in de plaats van bruglichten. In afwijking van het bepaalde in punt 39 zijn in dit geval de punten 9, 10, 12, 14, 17, 18, 19, 20, 21, 88, 89, 90, 97, 98 en 99 van overeenkomstige toepassing.
    
    In deze gevallen worden ten behoeve van het overige verkeer geen verkeerslichten toegepast. <br/> In de ruststand van een dergelijke regeling zijn de lichten van de tweekleurige verkeerslichten gedoofd.
44. Tweekleurige verkeerslichten mogen voorts worden toegepast in situaties waarin een incidenteel voorkomende verkeersstroom onderling regelen van conflicterende verkeersstromen met verkeerslichten tijdelijk noodzakelijk maakt en wel
    - bij het oversteken van een weg of rijbaan door langzaam verkeer;
    - bij het oversteken of afbuigen van of naar een weg of rijbaan door openbaar vervoer;
    - bij het oversteken van een weg of rijbaan door ander verkeer bij zeer lage verkeersintensiteiten;
    - bij het gebruik van een uitrit door openbaar vervoer.
    
    In deze gevallen worden op de hoofdrichting tweekleurige verkeerslichten toegepast en op de zijrichting al naar gelang het geval driekleurige verkeerslichten, driekleurige fietslichten, voetgangerslichten, tram/bus-lichten dan wel een combinatie van deze lichten.
    
    Zolang zich geen verkeer op de zijrichting heeft gemeld zijn de lichten van de tweekleurige verkeerslichten gedoofd en wordt in de op de zijrichting geplaatste verkeerslichten het rode licht getoond.

45. In andere situaties dan genoemd in de punten 43 en 44 worden tweekleurige verkeerslichten niet toegepast.

## Civilian
The max time of a traffic light showing a red sign is two minutes.

81. De frequentie van het knipperend groen bedraagt 80 tot 120 onderbrekingen per minuut met een licht-donkerverhouding van 1:1.
114. De frequentie van het knipperen bedraagt minimaal 40 en maximaal 60 onderbrekingen per minuut met een licht-donkerverhouding van 1:1. (Geel/Oranje licht)

## Public transit
![traffic states](./Docs/Readme/PublicTransitStates2.png)

56. De verkeerslantaarns van tram/bus-lichten zijn samengesteld uit twee, drie of vier witte lichten, een geel licht en twee rode lichten, die zijn aangebracht als aangegeven in bijlage II (Image above) behorende bij dit besluit.
59. De opsluitringen van de witte lichten moeten in grijs zijn uitgevoerd; die van de overige lichten in zwart.
68. Indien ter plaatse meerdere richtingen moeten worden geregeld wordt volstaan met één tram/bus-licht indien voor die richtingen altijd gelijktijdig wit, geel of rood licht wordt getoond. In andere gevallen wordt voor iedere richting dan wel gelijktijdig geregelde richtingen een afzonderlijk tram/bus-licht toegepast. Een en ander geschiedt overeenkomstig de afbeeldingen opgenomen in bijlage II (Image above), behorende bij dit besluit.
69. Indien aan rechtdoorgaand openbaar vervoer wit licht wordt getoond, moet aan kruisend en aan met een pijl geregeld, conflicterend verkeer rood licht worden getoond. Indien aan afbuigend openbaar vervoer wit licht wordt getoond, moet aan het verkeer dat wordt doorsneden, rood licht worden getoond.
70. Knipperend wit licht mag slechts worden toegepast in de volgende gevallen:
    - voor de onderlinge afwikkeling van openbaar vervoerbewegingen;
    - voor afslaande autobussen indien aan het rechtdoorgaande verkeer of afslaande verkeer, waaraan voorrang moet worden verleend, groen licht of geel licht wordt getoond;
    - indien de openbaar vervoerbeweging op een overigens geregelde kruising of splitsing van wegen een niet-geregelde voetgangersbeweging kruist.
71. De frequentie van het knipperend wit bedraagt 80 tot 120 onderbrekingen per minuut met een licht-donkerverhouding van 1:1.

## Train
Green is top, red is bottom

| Behaviour | Meaning 
| --- | --- 
| Green | Pursue
| Red | Stop 
| Amber (blinking) / Blinking Green | Slow down (optionally a speed limit)

## Some other stuff
![Controller](./Docs/Readme/Controller.png)


## Sources:
<https://nl.wikipedia.org/wiki/Verkeerslicht> <br/>
<http://www.onlinetheorieles.nl/theorieboek/27.html> <br/>
<http://wetten.overheid.nl/BWBR0009151/2010-03-31> <br/>
<http://wetten.overheid.nl/BWBR0004825/2017-07-01> <br/>

