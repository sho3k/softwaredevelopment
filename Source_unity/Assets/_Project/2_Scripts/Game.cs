﻿using System.Collections.Generic;
using System.Linq;
using Entitas;
using UnityEngine;

public class Game : MonoBehaviour
{
	static readonly List<IExecuteSystem> Updates;

	static Game()
	{
		Updates = new List<IExecuteSystem>();
	}

	public static int Team { get; private set; }

	public static CoreEntity CurrentEntity { get; private set; }

	public static Connection Current => CurrentEntity?.connection.Connection;

	public static CarBehaviour[] Vehicles { get; private set; }

	[SerializeField] CarBehaviour[] _vehicles;

	void Awake()
	{
		Vehicles = _vehicles;
	}

	void Start()
	{
		//Application.targetFrameRate = 30;

		AddComponent<SimulationFeature>()
			.AddComponent<UserInterfaceFeature>()
			.AddComponent<CoreFeature>()
			.AddComponent<TestFeature>();
	}
	
	void Update()
	{
		for (var i = 0; i < Updates.Count; i++)
			Updates[i].Execute();
	}

	Game AddComponent<T>() where T : MonoBehaviour
	{
		var obj = gameObject;
		obj.AddComponent<T>();
		return this;
	}

	[ContextMenu("Load Vehicles")]
	void LoadVehicles()
	{
		_vehicles = Resources.LoadAll<CarBehaviour>("");
		Debug.Log($"{_vehicles.Length} vehicles found!");
	}

	public static void RegisterUpdate(IExecuteSystem item)
	{
		Updates.Add(item);
	}

	public static void ClearTeam()
	{
		Team = 0;
		CurrentEntity = null;
		ConsoleBehaviour.Log("Team has been successfully set to null");
	}

	public static bool SetTeam(int team)
	{
		var connections = Contexts
			.sharedInstance
			.core
			.GetGroup(CoreMatcher.Connection)
			.GetEntities();

		var connection = connections.FirstOrDefault(c => c.connection.Team == team);
		if (connection == null)
			return false;

		Team = team;
		CurrentEntity = connection;
		
		ConsoleBehaviour.Log($"Team has been successfully set to {Team}");
		return true;
	}
}
