﻿using Entitas;

public class TestFeature : SystemBehaviour
{
	/// <inheritdoc />
	protected override Systems Construct(Contexts contexts)
	{
		return new Feature("Test");
	}
}
