﻿using Entitas;

public class UserInterfaceFeature : SystemBehaviour
{
	protected override Systems Construct(Contexts contexts)
	{
		var core = contexts.core;

		return new Feature("UI")
			.Add(new FadeSystem(core))
			.Add(new AlphaSystem(core))
			.Add(new CreateConsoleSystem(core))
			.Add(new ConsoleSystem());
	}
}
