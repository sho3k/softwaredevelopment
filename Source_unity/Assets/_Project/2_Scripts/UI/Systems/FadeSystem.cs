﻿using Entitas;
using UnityEngine;

public class FadeSystem : IExecuteSystem
{
	readonly IGroup<CoreEntity> _group;

	public FadeSystem(IContext<CoreEntity> context)
	{
		_group = context.GetGroup(CoreMatcher.AllOf(
			CoreMatcher.InterfaceAlpha,
			CoreMatcher.InterfaceFade));
	}

	/// <inheritdoc />
	public void Execute()
	{
		var deltaTime = Time.unscaledDeltaTime;

		foreach (var entity in _group.GetEntities())
		{
			var alpha = entity.interfaceAlpha.Alpha;
			var active = ConsoleSystem.Active;

			if (alpha <= 0 && !active ||
				alpha >= 1 && active)
				continue;

			alpha = active
				? alpha + deltaTime
				: alpha - deltaTime;

			entity.ReplaceInterfaceAlpha(alpha);
		}
	}
}