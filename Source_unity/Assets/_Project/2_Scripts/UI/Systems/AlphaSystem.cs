﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class AlphaSystem : ReactiveSystem<CoreEntity>
{
	/// <inheritdoc />
	public AlphaSystem(IContext<CoreEntity> context) : base(context)
	{
	}

	/// <inheritdoc />
	protected override ICollector<CoreEntity> GetTrigger(
		IContext<CoreEntity> context)
	{
		var group = context.GetGroup(
			CoreMatcher.AllOf(
				CoreMatcher.InterfaceAlpha,
				CoreMatcher.Asset));

		return group.CreateCollector();
	}

	/// <inheritdoc />
	protected override bool Filter(CoreEntity entity)
	{
		return entity.isEnabled;
	}

	/// <inheritdoc />
	protected override void Execute(List<CoreEntity> entities)
	{
		foreach (var entity in entities)
		{
			var obj = entity.asset.GameObject;
			var group = obj.GetComponent<CanvasGroup>();
			if (group == null)
				return;

			var alpha = entity.interfaceAlpha.Alpha;

			if (alpha > 0.0f)
			{
				group.alpha = alpha;
				obj.SetActive(true);
			}
			else
			{
				obj.SetActive(false);
				group.alpha = alpha;
			}
		}
	}
}
