﻿using System;
using System.Collections.Generic;
using System.Linq;
using Models;
using SimpleJSON;
using UnityEngine;
using Object = UnityEngine.Object;

public class ConsoleCommands
{
	Connection.Settings _host = Connection.Settings.Default;
	Action<byte[]> _stubMessage;

	static CoreEntity Create()
	{
		if (Game.CurrentEntity == null)
		{
			ConsoleBehaviour.Error("No active connection found!");
			return null;
		}

		var entity = Contexts.sharedInstance.core.CreateEntity();
		entity.AddTeam(Game.CurrentEntity);
		return entity;
	}


	

	public string echo(string message) => message;

	public double sum(string a, string b) => double.Parse(a) + double.Parse(b);
	public double sub(string a, string b) => double.Parse(a) - double.Parse(b);
	public double mul(string a, string b) => double.Parse(a) * double.Parse(b);
	
	public string host(string host)
	{
		if (host != null)
			_host.host = host;

		return $"Host set to: {_host.host}:{_host.port}";
	}

	public string port(string port)
	{
		if (port != null)
			_host.port = int.Parse(port);

		return $"Port set to: {_host.port}";
	}

	public string username(string username)
	{
		if (username != null)
			_host.username = username;

		return $"Username set to: {_host.username}";
	}

	public string password(string password)
	{
		if (password != null)
			_host.password = password;

		return $"Password set to: {_host.password}";
	}

	public object connect(string group)
	{
		var entity = Contexts.sharedInstance.core.CreateEntity();

		if (group == "stub")
		{
			if (_stubMessage != null)
			{
				return "Already connected to a stub";
			}

			_host.team = -1;
			entity.AddTime(0);
			entity.AddTimedelta(0);
			entity.AddTimescale(1);
			
			var dispatcher = new Dispatcher<Response>();
			entity.AddConnection(
				_host.StubConnect(dispatcher, out _stubMessage),
				dispatcher);
			entity.AddState(new List<Traffic>(0));

			Game.SetTeam(entity.connection.Team);

			return "stub";
		}


		_host.team = int.Parse(group);
		entity.AddCreateConnection(_host);
		return _host.ToJson(true);
	}

	public object disconnect(string group)
	{
		var team = group == "stub" ? -1 : int.Parse(group);
		var entities = Contexts.sharedInstance.core.GetEntities();

		if (Game.Current?.Host.team == team)
			Game.ClearTeam();

		var count = 0;
		for (var i = entities.Length - 1; i >= 0; i--)
		{
			var entity = entities[i];

			if (entity.hasTeam && entity.team.Team == team)
			{
				entity.Destroy();
				count++;
			}
		}
		
		return $"{count} entities have been destroyed";
	}

	public object emit(string message)
	{
		Game.Current.Emit(message.Bytes());
		return null;
	}
	
	public object light(string lightId, string state, string time)
	{
		if (state == null)
		{
			var id = int.Parse(lightId);
			var core = Contexts.sharedInstance.core;
			var entities = core.GetGroup(CoreMatcher.LightState).GetEntities();
			var entity = entities.FirstOrDefault(x => x.lightState.Id == id);
			if (entity == null)
				return "undefined";

			var ls = entity.lightState;
			return $"State: {ls.Status} : {ls.Time}";
		}


		int target;
		var s = int.Parse(state);
		var t = time == null
			? -1
			: float.Parse(time);
		if (int.TryParse(lightId, out target))
		{
			var entity = Create();
			entity.AddLightState(target, s, t);
		}
		else
		{
			var enumValue = lightId == "_"
				? VehicleType.Void
				: (VehicleType)Enum.Parse(typeof(VehicleType), lightId, true);

			var lights = Object.FindObjectsOfType<TrafficLightBehaviour>();
			foreach (var trafficLightBehaviour in lights)
			{
				if (enumValue != VehicleType.Void)
				{
					var idt = trafficLightBehaviour.Id / 100;
					switch (idt)
					{
						case BikeNode.Id:
							if (enumValue == VehicleType.Bike)
								break;
							continue;


						case TrainNode.CrossingId:
						case CarNode.Id:
							if (enumValue == VehicleType.Car)
								break;
							continue;


						case BusNode.Id:
							if (enumValue == VehicleType.Bus)
								break;
							continue;


						case PedestrianNode.Id:
							if (enumValue == VehicleType.Pedestrian)
								break;
							continue;

							default: continue;
					}
				}

				var entity = Create();
				entity.AddLightState(trafficLightBehaviour.Id, s, t);
			}
		}
		return null;
	}

	public object path(string value)
	{
		var on = StringToBool(value);

		var nodes = GameObject.FindObjectsOfType<Node>();
		foreach (var node in nodes)
		{
			var visualizer = node.GetComponent<NodeVisualizer>();
			if (visualizer == null)
				continue;

			visualizer.enabled = on;
		}
		return null;
	}

	public object create(string type, string street_name)
	{
		Node start = null;
		var enumValue = (VehicleType)Enum.Parse(typeof(VehicleType), type, true);
		

		if (street_name != null)
		{
			street_name = street_name.ToLower();

			var spawnNodes = Object.FindObjectsOfType<SpawnNode>();
			foreach (var spawnNode in spawnNodes)
			{
				var node = spawnNode.Node;
				if (!InitializeVehicleSystem.HasType(enumValue, node))
					continue;

				var street = node.transform.parent.name;
				if (street.ToLower() != street_name)
					continue;
				
				start = node;
				break;
			}

			if (start == null)
				throw new Exception("Could not find a node with the street name");
		}

		var entity = Create();
		entity.AddVehicle(enumValue);

		if (start != null)
			entity.AddTarget(start);

		return null;
	}

	public object spawn(
		string state,
		string type,
		string min_rate,
		string max_rate)
	{
		var on = StringToBool(state);
		var enumValue = type == null ? VehicleType.Void : (VehicleType)Enum.Parse(typeof(VehicleType), type, true);
		var minRate = min_rate == null ? -1 : float.Parse(min_rate);
		var maxRate = max_rate == null ? -1 : float.Parse(max_rate);

		var group = Contexts.sharedInstance.core.GetGroup(CoreMatcher.VehicleSpawner);

		foreach (var entity in group.GetEntities())
		{
			if (enumValue == VehicleType.Void || entity.vehicle.Type == enumValue)
			{
				if (minRate >= 0)
				{
					entity.vehicleSpawner.MinRate = minRate;
				}

				if (maxRate >= 0)
				{
					entity.vehicleSpawner.MaxRate = maxRate;
				}

				entity.vehicleSpawner.MaxRate = on
					? Mathf.Abs(entity.vehicleSpawner.MaxRate)
					: -Mathf.Abs(entity.vehicleSpawner.MaxRate);
			}
		}

		return null;
	}

	public object speed(string scale)
	{
		// Create JSON
		var value = float.Parse(scale);
		var obj = new JSONObject();
		obj["Speed"] = value;

		// Create request
		var entity = Create();
		var json = obj.ToString();
		entity.AddRequest(json);
		return value;
	}

	public object exit()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
		return null;
	}

	public string help()
	{
		// TODO: Create attribute with description

		return @"echo(message) -> returns message
sum(a, b) -> a + b
sub(a, b) -> a - b
mul(a, b) -> a * b

host() -> current host and port
host(host) -> sets host <example=""10.0.2.1""/>

port() -> current port
port() -> sets port <example=""1234""/>
			
connect(team) -> connects to the defined team id <example=""2""/>
disconnect(team) -> removes all entities containing the defined team id <example=""2""/>

emit(message) -> sends raw data to the connected team

light(id) -> get the current state of the defined light <example=""102""/>
light(id, state) -> set the current state of the defined light <example=""102, 2""/>
light(id, state, wait) -> set the current state of the defined light and its wait time <example=""102 2 23.43""/>

path(on) -> set the visability of debug paths off(off, 0, false) on(on, 1, true). <remarks=""Not optimized +-200kB/f garbage""/>

create(type) -> create [car, pedestrian, bike, bus, train] at a random entry node

speed(scale) -> request a speed, recommended to stay below 5: AI is quite expensive to call and a high timestep will cause multiple passes";
	}

	static bool StringToBool(string s)
	{
		switch (s)
		{
			case "1":
			case "true":
			case "on":
				return true;

			case "0":
			case "false":
			case "off":
				return false;

			default:
				throw new ArgumentException($"{s} is not a valid argument");
		}
	}
}