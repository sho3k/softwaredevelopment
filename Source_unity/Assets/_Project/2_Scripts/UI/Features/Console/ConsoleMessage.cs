﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ConsoleMessage : MonoBehaviour, IPointerClickHandler
{
	[SerializeField] Text _command;
	[SerializeField] Text _message;
	[SerializeField] Text _inner;

	/// <inheritdoc />
	public void OnPointerClick(PointerEventData eventData)
	{
		if (_inner.text.Length > 0)
			_inner.gameObject.SetActive(!_inner.gameObject.activeSelf);
	}

	public void Initialize(string command, string message, string inner,
		LogType type)
	{
		_command.text = command;
		_message.text = message;
		_message.color = GetColor(type);
		_inner.text = inner;

		_command.gameObject.SetActive(!string.IsNullOrEmpty(command));
		_message.gameObject.SetActive(!string.IsNullOrEmpty(message));
		_inner.gameObject.SetActive(false);
	}

	static Color GetColor(LogType type)
	{
		switch (type)
		{
			case LogType.Error: return new Color(1.0f, 0.5f, 0.1f);
			case LogType.Assert: return new Color(1.0f, 0.1f, 0.5f);
			case LogType.Warning: return new Color(1.0f, 0.7f, 0.3f);
			case LogType.Log: return new Color(0.4f, 0.7f, 1.0f);
			case LogType.Exception: return new Color(1.0f, 0.0f, 0.0f);
			default: return Color.white;
		}
	}
}
