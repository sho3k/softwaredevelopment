﻿using Entitas;
using UnityEngine;

public class ConsoleSystem : IInitializeSystem, IExecuteSystem
{
	public static bool Active;

	/// <inheritdoc />
	public void Initialize()
	{
		Active = false;
	}

	/// <inheritdoc />
	public void Execute()
	{
		if (Input.GetKeyUp(KeyCode.BackQuote))
			Active = !Active;
	}
}
