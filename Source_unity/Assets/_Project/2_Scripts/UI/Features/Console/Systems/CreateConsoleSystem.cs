﻿using Entitas;
using UnityEngine;

public class CreateConsoleSystem : IInitializeSystem
{
	readonly IContext<CoreEntity> _core;

	public CreateConsoleSystem(IContext<CoreEntity> core)
	{
		_core = core;
	}
	
	/// <inheritdoc />
	public void Initialize()
	{
		const float fadeDuration = 0.5f;

		var entity = _core.CreateEntity();
		entity.isConsole = true;
		entity.AddInterfaceAlpha(0);
		entity.AddInterfaceFade(fadeDuration);

		var obj = Resources.Load<GameObject>("Console");
		obj = Object.Instantiate(obj);
		entity.AddAsset(obj.transform, obj);
	}
}
