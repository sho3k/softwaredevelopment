﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ConsoleBehaviour : MonoBehaviour
{
	static readonly Dictionary<string, MethodInfo> Commands;
	static readonly StringBuilder Builder;
	static readonly List<MethodInfo> Cache;
	public static ConsoleBehaviour Instance;

	[SerializeField] InputField _field;
	[SerializeField] ConsoleMessage _original;
	[SerializeField] RectTransform _container;
	[SerializeField] int _maxLines = 24;
	EventSystem _eventSystem;
	ConsoleCommands _console;
	List<string> _commands;
	string _cache;
	int _child = -1;

	static ConsoleBehaviour()
	{
		Builder = new StringBuilder();
		Commands = new Dictionary<string, MethodInfo>();
		Commands = typeof(ConsoleCommands).GetMethods(
											BindingFlags.Static |
											BindingFlags.Instance |
											BindingFlags.Public |
											BindingFlags.DeclaredOnly)
										.ToDictionary(k => k.Name);

		Cache = new List<MethodInfo>(24);
	}

	void Awake()
	{
		Instance = this;
		_console = new ConsoleCommands();
		_commands = new List<string>(24);

		_eventSystem = FindObjectOfType<EventSystem>();
		_field.onValidateInput += SubmitChecker;
	}

	char SubmitChecker(string text, int i, char c)
	{
		switch (c)
		{
			case '\0':
			case '\n':
			case '\r':
				_field.text = "";
				Command(text);
				return '\0';

			case ' ':
			case '.':
			case '_':
				return text.Length > 0 && text[text.Length - 1] == c
					? '\0'
					: c;

			case '\t':
				Cache.Clear();
				foreach (var command in Commands)
				{
					if (command.Key.StartsWith(text))
						Cache.Add(command.Value);
				}

				if (Cache.Count == 1)
					_field.text = Cache[0].Name + ' ';
				else
				{
					Builder.Length = 0;

					foreach (var command in Cache)
					{
						Builder
							.Append(command.Name)
							.Append('(')
							.Append(string.Join(
								", ",
								command.GetParameters()
									.Select(p => p.Name)))
							.Append(')')
							.Append('\n');
					}

					Log(Builder.ToString());
				}

				return '\0';


			case '{':
			case '}':
				return c;
		}

		c = char.ToLower(c);
		if (c >= '0' && c <= '9')
			return c;

		if (c >= 'a' && c <= 'z')
			return c;

		return '\0';
	}

	void LateUpdate()
	{
		_field.Select();
		_eventSystem.SetSelectedGameObject(_field.gameObject);
		
		if (Input.GetKeyDown(KeyCode.UpArrow))
			MoveCommand(-1);
		else if (Input.GetKeyDown(KeyCode.DownArrow))
			MoveCommand(1);
	}

	void MoveCommand(int amount)
	{
		var count = _commands.Count;
		if (count < 1)
			return;

		if (_child == count)
			_cache = _field.text;

		_child = Mathf.Clamp(_child + amount, 0, count);
		_field.text = _child == count
			? _cache
			: _commands[_child];
	}
	
	ConsoleMessage Create()
	{
		var count = _container.childCount;
		if (count < _maxLines)
			return Instantiate(_original, _container, false);

		var first = _container.GetChild(0);
		first.SetSiblingIndex(count - 1);

		return first.GetComponent<ConsoleMessage>();
	}

	public static void Error(string message, string inner = null)
	{
		Log(null, message, inner, LogType.Error);
	}

	public static void Exception(Exception exception, string command = null)
	{
		var message = exception.Message;

		Builder.Length = 0;
		while (exception != null)
		{
			Builder
				.Append(exception)
				.Append("\n\n");

			exception = exception.InnerException;
		}

		Log(command, message, Builder.ToString(), LogType.Exception);
	}

	public static void Log(string message, string inner = null)
	{
		Instance.Create().Initialize(null, message, inner, LogType.Log);
	}

	public static void Log(string command, string message, string inner, LogType type)
	{
		Instance.Create().Initialize(command, message, inner, type);
	}

	void OnEnable()
	{
		_field.text = "";
		_field.ActivateInputField();
	}

	void InitializePush(bool userInput, string text)
	{
		Builder.Length = 0;

		Builder
			.Append(userInput
				? '>'
				: '\t')
			.Append(' ')
			.Append(text);

		if (!userInput)
			Builder.AppendLine();
	}
	
	public void Command(string command)
	{
		var prms = command.Split(' ');
		if (string.IsNullOrWhiteSpace(command) || prms.Length < 1)
			return;

		_commands.Add(command);
		_child = _commands.Count;


		try
		{
			var info = Commands[prms[0]];
			var paramCount = info.GetParameters().Length;
			var inpp = new object[paramCount];
			
			for (var i = 0; i < prms.Length - 1; i++)
				inpp[i] = prms[i + 1];

			var result = info.Invoke(info.IsStatic
				? null
				: _console, inpp);

			Log(command, result?.ToString(), null, LogType.Log);
		}
		catch (Exception e)
		{
			Exception(e, command);
		}
	}
}
