﻿using UnityEngine;

public class FreeCamera : MonoBehaviour
{
	public Vector2 PadLookSensitivity = new Vector2(2,2);
	public Vector2 PcLookSensitivity = new Vector2(2,2);
	public float LookSmoothing = 0.1f;
	public float MoveSpeed = 5.0f;
	public float SprintSpeed = 15.0f;
	public float MoveSmoothing = 0.1f;
	
	//GamePad _gamePad;
	Vector2 _angle;
	Vector3 _position;
	Vector2 _lookVelocity;
	Vector3 _moveVelocity;
	
	void Awake()
	{
		//_gamePad = GamePad.Construct();
	}

	void LateUpdate()
	{
		if (ConsoleBehaviour.Instance != null && ConsoleBehaviour.Instance.gameObject.activeSelf)
			return;
		
		var look = Look() + _angle;
		_angle = Vector2.SmoothDamp(_angle, look, ref _lookVelocity, LookSmoothing, Mathf.Infinity, Time.deltaTime); /*new Vector2(
			Mathf.SmoothDampAngle(
				_angle.x, look.x,
				ref _lookVelocity.x,
				LookSmoothing),
			Mathf.SmoothDampAngle(
				_angle.y, look.y,
				ref _lookVelocity.y,
				LookSmoothing));*/

		_position = Vector3.SmoothDamp(
			_position,
			_position + transform.TransformVector(Move()),
			ref _moveVelocity,
			MoveSmoothing,
			Mathf.Infinity,
			Time.deltaTime);

		transform.localPosition = _position;
		transform.localRotation = Quaternion.Euler(_angle.y, _angle.x, 0);
	}

	Vector2 Look()
	{
		//var pad = _gamePad.RightStick;
		//pad.Scale(PadLookSensitivity);

		var pc = new Vector2(
			Input.GetAxisRaw("Mouse X"),
			Input.GetAxisRaw("Mouse Y"));
		pc.Scale(PcLookSensitivity);

		return pc;
		//return pad + pc;
	}

	Vector3 Move()
	{
		return (Input.GetButton("Fire3")
			? SprintSpeed
			: MoveSpeed) * new Vector3(
				Input.GetAxisRaw("Horizontal"),
				Input.GetAxisRaw("EQ"),
				Input.GetAxisRaw("Vertical"));

		/*return (Input.GetButton("Fire3") || (_gamePad.State & GamePadKey.LeftShoulder) != 0
			? SprintSpeed
			: MoveSpeed) * (new Vector3(
				_gamePad.LeftStick.x,
				_gamePad.LeftStick.y,
				_gamePad.RightTrigger - _gamePad.LeftTrigger)
			+ new Vector3(
				Input.GetAxisRaw("Horizontal"),
				Input.GetAxisRaw("EQ"),
				Input.GetAxisRaw("Vertical")));*/
	}
}
