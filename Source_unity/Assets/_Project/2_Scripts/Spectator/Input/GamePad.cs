﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Entitas;
using UnityEngine;

public class GamePad : IExecuteSystem
{
	const string DllName = "XInputInterface";
	const uint ExitSuccess = 0x0;

	
	#if UNITY_EDITOR_LINUX
	static uint XInputGamePadGetState(uint playerIndex, PadState state)
	{
		return ~ExitSuccess;
	}

	static void XInputGamePadSetState(uint playerIndex, float leftMotor, float rightMotor){}
	#else
	[DllImport(DllName)]
	static extern uint XInputGamePadGetState(uint playerIndex, PadState state);

	[DllImport(DllName)]
	static extern void XInputGamePadSetState(uint playerIndex, float leftMotor, float rightMotor);
	#endif
	static readonly Dictionary<uint, GamePad> Pads = new Dictionary<uint, GamePad>();

	[StructLayout(LayoutKind.Sequential)]
	class PadState
	{
		public uint DwPacketNumber;
		public GamePad Gamepad;

		[StructLayout(LayoutKind.Sequential)]
		public struct GamePad
		{
			public ushort DwButtons;
			public byte BLeftTrigger;
			public byte BRightTrigger;
			public short SThumbLx;
			public short SThumbLy;
			public short SThumbRx;
			public short SThumbRy;
		}
	}

	readonly uint Id;
	readonly PadState _state;

	public bool Connected { get; private set; }
	public GamePadKey State { get; private set; }
	public Vector2 LeftStick { get; private set; }
	public Vector2 RightStick { get; private set; }
	public float LeftTrigger { get; private set; }
	public float RightTrigger { get; private set; }

	GamePad(uint id = 0u)
	{
		Id = id;
		_state = new PadState();
	}

	void IExecuteSystem.Execute()
	{
		// Get state
		var result = XInputGamePadGetState(Id, _state);

		Connected = result == ExitSuccess;

		if (Connected)
			UpdateState();
		else
			ResetState();
	}

	void ResetState()
	{
		State = GamePadKey.None;
		LeftStick = Vector2.zero;
		RightStick = Vector2.zero;
		LeftTrigger = 0;
		RightTrigger = 0;
	}

	void UpdateState()
	{
		LeftStick = new Vector2(
			_state.Gamepad.SThumbLx,
			_state.Gamepad.SThumbLy);

		RightStick = new Vector2(
			_state.Gamepad.SThumbRx,
			_state.Gamepad.SThumbRy);

		LeftTrigger = _state.Gamepad.BLeftTrigger;
		RightTrigger = _state.Gamepad.BRightTrigger;

		LeftStick /= short.MaxValue;
		RightStick /= short.MaxValue;
		LeftStick /= byte.MaxValue;
		RightTrigger /= byte.MaxValue;
		
		// Update state
		uint state = _state.Gamepad.DwButtons;
		State = (GamePadKey)state;
	}

	public static GamePad Construct(uint id = 0u)
	{
		GamePad pad;
		if (Pads.TryGetValue(id, out pad))
			return pad;

		pad = new GamePad(id);
		Pads.Add(id, pad);
		Game.RegisterUpdate(pad);
		return pad;
	}
}