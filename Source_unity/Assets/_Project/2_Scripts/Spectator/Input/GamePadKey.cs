﻿using System;

[Flags]
public enum GamePadKey : uint
{
    All             = uint.MaxValue,
    None            = uint.MinValue,

    DPadUp          = 1 << 00,
    DPadDown        = 1 << 01,
    DPadLeft        = 1 << 02,
    DPadRight       = 1 << 03,

    Start           = 1 << 04,
    Back            = 1 << 05,
    Guide           = 1 << 10,

    LeftStick       = 1 << 06,
    RightStick      = 1 << 07,

    LeftShoulder    = 1 << 08,
    RightShoulder   = 1 << 09,

    A               = 1 << 12,
    B               = 1 << 13,
    X               = 1 << 14,
    Y               = 1 << 15,

    LeftStickUp     = 1 << 16,
    LeftStickDown   = 1 << 17,
    LeftStickLeft   = 1 << 18,
    LeftStickRight  = 1 << 19,

    RightStickUp    = 1 << 20,
    RightStickDown  = 1 << 21,
    RightStickLeft  = 1 << 22,
    RightStickRight = 1 << 23,
    
    LeftTrigger     = 1 << 24,
    RightTrigger    = 1 << 25
}
