﻿using UnityEngine;
using UnityEngine.EventSystems;

public class MinimapResizer : MonoBehaviour, IDragHandler
{
	[SerializeField] RectTransform _viewport;
	[SerializeField] Rect _bounds;
	
	/// <inheritdoc />
	public void OnDrag(PointerEventData eventData)
	{
		var position = eventData.position;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(
			_viewport,
			position,
			eventData.pressEventCamera,
			out position);

		// Clamp
		position.x = Mathf.Clamp(position.x, _bounds.xMin, _bounds.xMax);
		position.y = Mathf.Clamp(position.y, _bounds.yMin, _bounds.yMax);

		// Apply position
		_viewport.offsetMin = position;
	}
}
