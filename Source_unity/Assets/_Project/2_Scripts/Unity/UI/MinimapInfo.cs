﻿using UnityEngine;
using UnityEngine.UI;

public class MinimapInfo : MonoBehaviour
{
	TrafficLightBehaviour _light;

	void Awake()
	{
		_light = GetAncestorComponent<TrafficLightBehaviour>(transform);
	}

	public void Visit(Text text)
	{
		text.text = _light.Id.ToString();
	}

	static T GetAncestorComponent<T>(Transform transform) where T : class
	{
		if (transform == null)
			return null;

		var component = transform.GetComponent<T>();
		return component ?? GetAncestorComponent<T>(transform.parent);
	}
}
