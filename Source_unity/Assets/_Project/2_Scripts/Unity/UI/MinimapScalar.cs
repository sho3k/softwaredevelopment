﻿using UnityEngine;
using UnityEngine.EventSystems;

public class MinimapScalar : MonoBehaviour, IScrollHandler
{
	[SerializeField]
	RectTransform _transform;

	Vector2 _size;
	Vector2 _scalar;

	/// <inheritdoc />
	public void OnScroll(PointerEventData eventData)
	{
		var scroll = eventData.scrollDelta;
		_size += _scalar * scroll.y;
		
		var center = (_transform.offsetMax + _transform.offsetMin) / 2.0f;
		
		_transform.offsetMin = center - _size;
		_transform.offsetMax = center + _size;
	}

	void Awake()
	{
		_size = (_transform.offsetMax - _transform.offsetMin) / 2.0f;
		_scalar = _size * 0.1f;
	}
}
