﻿using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MinimapDetails : MonoBehaviour, IPointerClickHandler
{
	[SerializeField] Camera _minimap;
	[SerializeField] Text _text;

	public new RectTransform transform => base.transform as RectTransform;

	void Start()
	{
		if (_minimap == null)
			_minimap = FindObjectsOfType<Camera>().First(x => x.targetTexture != null);
	}

	/// <inheritdoc />
	public void OnPointerClick(PointerEventData eventData)
	{
		var position = eventData.position;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(
			transform,
			position,
			eventData.pressEventCamera,
			out position);
			
		var rect = transform.rect;
		position -= rect.min;
		position.x /= rect.size.x;
		position.y /= rect.size.y;
		
		if (position.x < 0 || position.x > 1 ||
			position.y < 0 || position.y > 1)
			return;

		RaycastHit hit;
		var ray = _minimap.ViewportPointToRay(position);
		Physics.Raycast(ray, out hit, Mathf.Infinity, _minimap.cullingMask);

		Debug.Log(hit.collider);
		if (hit.collider == null)
		{
			_text.text = "";
			return;
		}

		var info = hit.collider.GetComponent<MinimapInfo>();
		if (info != null)
			info.Visit(_text);
	}
}
