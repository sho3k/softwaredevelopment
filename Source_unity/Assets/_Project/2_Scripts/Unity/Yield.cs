﻿using System;
using System.Collections;
using UnityEngine;

public class Yield : Controller<Yield>
{
	public static void Invoke(Action action, float wait)
	{
		instance.StartCoroutine(
			Invoke(
				action,
				new WaitForSeconds(wait)));
	}

	static IEnumerator Invoke(Action action, YieldInstruction instruction)
	{
		yield return instruction;

		action();
	}
}
