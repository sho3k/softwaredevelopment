﻿using System.Linq;
using UnityEngine;

public class Controller<T> : UnityBehaviour where T : UnityBehaviour
{
	static T _instance;
	public static T instance => _instance ?? CreateInstance();

	public static bool Instantiated { get; private set; }
	public static bool Destroyed { get; private set; }

	/// <summary>
	///     Initialize instance
	/// </summary>
	/// <returns></returns>
	static T CreateInstance(T t = null)
	{
		if (Destroyed)
			return null;

		if (_instance != null || Instantiated)
			return _instance;

		_instance = t;
		if (_instance == null)
		{
			// Find object in scene
			var instances = FindObjectsOfType<T>();
			if (instances.Length > 0)
			{
				// There are/is already instance(s) of type
				if (instances.Length > 1)
				{
					// Delete the remaining objects
					Debug.LogWarning(
						$"[Singleton] There is more than one instance of \"{typeof(T)}\". \"{string.Join(", ", instances.Select(x => x.name))}\" are destroyed to continue.");


					for (var i = 1; i < instances.Length; i++) Destroy(instances[i].gameObject);
				}

				_instance = instances[0];
			}
			else
			{
				// Preventing creating multiple objects
				Instantiated = true;

				// Create new Gameobject with the 
				var obj = new GameObject();
				_instance = obj.AddComponent<T>();
				obj.name = typeof(T) + " (singleton)";

				// Notify
				Debug.Log(
					$"[Singleton] An instance of {typeof(T)} is needed in the scene, so \"{obj.name}\" was created to continue.");

				// Reset value
				Instantiated = false;
			}
		}

		// Finalize
		Destroyed = false;
		DontDestroyOnLoad(_instance.gameObject);
		Instantiated = true;
		return _instance;
	}

	/// <summary>
	///     Called bevore first frame when creaded
	/// </summary>
	void Awake()
	{
		// Instantiate if not already done
		if (_instance == null)
		{
			if (!Instantiated) CreateInstance();
			OnAwake();
			return;
		}

		// Destroy object because another one already excists
		if (_instance.GetInstanceID() != GetInstanceID())
		{
			Debug.LogWarning(
				$"[Singleton] Deleting \"{gameObject.name}\", because instance \"{_instance.gameObject.name}\" is already found");

			Destroy(gameObject);
			return;
		}

		OnAwake();
	}

	/// <summary>
	///     Called when object is going to be destroyed
	/// </summary>
	void OnDestroy()
	{
		Destroyed = true;
		Instantiated = false;
		OnDestroying();
	}

	/// <summary>
	///     Called bevore first frame when creaded
	/// </summary>
	protected virtual void OnAwake()
	{
	}

	/// <summary>
	///     Called when object is being destroyed
	/// </summary>
	protected virtual void OnDestroying()
	{
	}
}
