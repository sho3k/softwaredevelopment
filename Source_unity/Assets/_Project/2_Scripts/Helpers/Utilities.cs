﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class Utilities
{
	public static string Utf8(this byte[] bytes)
	{
		return Encoding.UTF8.GetString(bytes);
	}

	public static byte[] Bytes(this string str)
	{
		return Encoding.UTF8.GetBytes(str);
	}

	public static bool Contains(this string s, char c)
	{
		for (var i = 0; i < s.Length; i++)
			if (s[i] == c)
				return true;

		return false;
	}

	public static string ToJson(this object obj, bool pretty = false)
	{
		return JsonUtility.ToJson(obj, pretty);
	}

	/// <summary>
	///     bool ? 1 : -1
	/// </summary>
	/// <param name="b"></param>
	/// <returns></returns>
	public static int ToMul(this bool b)
	{
		return b
			? 1
			: -1;
	}

	/// <summary>
	///     bool ? 1 : 0
	/// </summary>
	/// <param name="b"></param>
	/// <returns></returns>
	public static int To10(this bool b)
	{
		return b
			? 1
			: 0;
	}

	public static T Random<T>(this IList<T> list)
	{
		if (list == null)
			throw new ArgumentNullException(nameof(list));
			
		return list.Count == 0
			? default(T)
			: list[UnityEngine.Random.Range(0, list.Count)];
	}
}
