﻿using Entitas;
using UnityEngine;

public abstract class SystemBehaviour : MonoBehaviour
{
	Systems _systems;

	protected abstract Systems Construct(Contexts contexts);

	void Awake()
	{
		_systems = Construct(Contexts.sharedInstance);
	}

	void Start()
	{
		_systems.Initialize();
	}

	void Update()
	{
		_systems.Execute();
		_systems.Cleanup();
	}

	void OnDestroy()
	{
		_systems.TearDown();
	}
}
