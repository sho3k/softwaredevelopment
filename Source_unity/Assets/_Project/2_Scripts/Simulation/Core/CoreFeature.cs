﻿using Entitas;

public class CoreFeature : SystemBehaviour
{
	protected override Systems Construct(Contexts contexts)
	{
		var context = contexts.core;
		return new Feature("Core")
			.Add(new PositionSystem(context))
			.Add(new RotationSystem(context))
			.Add(new EulerAngleSystem(context));
	}
}
