﻿using Entitas;
using UnityEngine;

[Core]
public class AssetComponent : IComponent
{
	public Transform Transform;
	public GameObject GameObject;
}
