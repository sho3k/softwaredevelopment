﻿using Entitas;
using UnityEngine;

[Core]
public class RotationComponent : IComponent
{
	public Quaternion Value;
}
