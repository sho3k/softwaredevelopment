﻿using Entitas;
using UnityEngine;

[Core]
public class PositionComponent : IComponent
{
	public Vector3 Value;
}
