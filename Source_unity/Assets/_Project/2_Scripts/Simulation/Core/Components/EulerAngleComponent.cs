﻿using Entitas;
using UnityEngine;

[Core]
public class EulerAngleComponent : IComponent
{
	public Vector3 Value;
}
