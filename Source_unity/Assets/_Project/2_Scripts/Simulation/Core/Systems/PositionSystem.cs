﻿using System.Collections.Generic;
using Entitas;

public class PositionSystem : ReactiveSystem<CoreEntity>
{
	/// <inheritdoc />
	public PositionSystem(IContext<CoreEntity> context) : base(context)
	{
	}

	/// <inheritdoc />
	protected override ICollector<CoreEntity> GetTrigger(
		IContext<CoreEntity> context)
	{
		var group = context.GetGroup(
			CoreMatcher.AllOf(
				CoreMatcher.Position,
				CoreMatcher.Asset));

		return group.CreateCollector();
	}

	/// <inheritdoc />
	protected override bool Filter(CoreEntity entity)
	{
		return entity.isEnabled;
	}

	/// <inheritdoc />
	protected override void Execute(List<CoreEntity> entities)
	{
		foreach (var entity in entities)
		{
			var transform = entity.asset.Transform;
			var position = entity.position.Value;
			transform.localPosition = position;
		}
	}
}
