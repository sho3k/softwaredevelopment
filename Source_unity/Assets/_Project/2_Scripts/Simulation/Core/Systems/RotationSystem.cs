﻿using System.Collections.Generic;
using Entitas;

public class RotationSystem : ReactiveSystem<CoreEntity>
{
	/// <inheritdoc />
	public RotationSystem(IContext<CoreEntity> context) : base(context)
	{
	}

	/// <inheritdoc />
	protected override ICollector<CoreEntity> GetTrigger(
		IContext<CoreEntity> context)
	{
		var group = context.GetGroup(
			CoreMatcher.AllOf(
				CoreMatcher.Rotation,
				CoreMatcher.Asset));

		return group.CreateCollector();
	}

	/// <inheritdoc />
	protected override bool Filter(CoreEntity entity)
	{
		return entity.isEnabled;
	}

	/// <inheritdoc />
	protected override void Execute(List<CoreEntity> entities)
	{
		foreach (var entity in entities)
		{
			var transform = entity.asset.Transform;
			var rotation = entity.rotation.Value;
			transform.localRotation = rotation;
		}
	}
}
