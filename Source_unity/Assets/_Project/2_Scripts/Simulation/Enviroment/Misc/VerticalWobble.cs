﻿using UnityEngine;

public class VerticalWobble : MonoBehaviour
{
	const float MinSpeed = 1.2f;
	const float MaxSpeed = 1.6f;
	const float Amplitude = 0.05f;
	const float MaxRotation = 0.03f;

	[SerializeField] float _intensity = 1.0f;

	Vector3 _startPosition;
	Vector3 _up;
	Quaternion _startRotation;
	Quaternion _targetRotation;

	float _speed;
	float _offset;

	void Awake()
	{
		_startPosition = transform.localPosition;
		_up = transform.InverseTransformDirection(0, 1, 0);
		_speed = Random.Range(MinSpeed, MaxSpeed);
		_offset = Random.value * 1000;

		var startRotation = transform.localRotation;
		_startRotation = Quaternion.Lerp(startRotation, Random.rotationUniform, MaxRotation * _intensity);
		_targetRotation = Quaternion.Lerp(startRotation, Random.rotationUniform, MaxRotation * _intensity);
	}
	
	void Update()
	{
		OnGameUpdated(Time.time);
	}

	public void OnGameUpdated(float time)
	{
		var t = Mathf.Sin(time * _speed + _offset);
		transform.localPosition = _startPosition + _up * (t * Amplitude * _intensity);
		transform.localRotation = Quaternion.SlerpUnclamped(_startRotation, _targetRotation, t);
	}
}
