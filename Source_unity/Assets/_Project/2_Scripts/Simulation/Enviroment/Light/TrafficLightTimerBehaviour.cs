﻿using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class TrafficLightTimerBehaviour : MonoBehaviour
{
	static readonly StringBuilder _builder = new StringBuilder(8);
	static TrafficLightTimerBehaviour _original;

	[SerializeField] Text[] _texts;
	float _lastTime;
	float _time;


	public static TrafficLightTimerBehaviour CreateInstance(Transform parent, float time)
	{
		if (_original == null)
			_original = Resources.Load<TrafficLightTimerBehaviour>("TrafficLightTimer");
		
		var current = Instantiate(_original, parent, false);
		current.SetTime(time);

		var canvas = current.GetComponent<Canvas>();
		canvas.worldCamera = Camera.main;

		return current;
	}

	public void SetTime(float time)
	{
		ApplyTime(time);

		// Cache time
		_time = time;
		var entity = Game.CurrentEntity;
		if (entity != null)
			_lastTime = entity.time.Time;
	}

	void ApplyTime(float time)
	{
		if (time < 0.05f)
		{
			gameObject.SetActive(false);
			return;
		}

		_builder.Length = 0;
		_builder.AppendFormat("{0:0.0}", time);
		var content = _builder.ToString();
		foreach (var text in _texts)
			text.text = content;

		gameObject.SetActive(true);
	}

	void Update()
	{
		var entity = Game.CurrentEntity;
		if (entity == null)
			return;

		var time = entity.time.Time;
		time -= _lastTime;
		time = _time - time;

		ApplyTime(time);
	}
}
