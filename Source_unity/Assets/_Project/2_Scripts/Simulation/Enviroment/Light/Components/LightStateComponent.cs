﻿using System;
using Entitas;

[Core, Serializable]
public class LightStateComponent : IComponent
{
	public int Id;
	public int Status;
	public float Time;
}
