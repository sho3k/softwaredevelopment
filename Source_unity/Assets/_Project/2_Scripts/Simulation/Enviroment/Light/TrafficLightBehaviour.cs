﻿using UnityEngine;

public abstract class TrafficLightBehaviour : MonoBehaviour
{
	[SerializeField] int _id;
	TrafficLightTimerBehaviour _timer;

	public int Id => _id;

	/// <summary>
	///     Set the state of the light
	/// </summary>
	/// <param name="stateId">
	///     Determines the behaviour of the lights
	///     See docs for all meanings.
	/// </param>
	public abstract void SetState(int stateId);

	public void SetTime(float time)
	{
		if (_timer != null)
			_timer.SetTime(time);
		else if (time > 0)
			_timer = TrafficLightTimerBehaviour.CreateInstance(transform, time);
	}
}
