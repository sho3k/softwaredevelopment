﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public abstract class TrafficLightBulbBehaviour : MonoBehaviour
{
	const float OnStrength = 2.5f;
	const float OffStrength = 0.1f;


	MaterialPropertyBlock _off;
	MaterialPropertyBlock _on;
	MeshRenderer _renderer;
	Coroutine _blinking;

	bool _isOn;
	public bool IsOn
	{
		get { return _isOn; }
		set
		{
			_isOn = value;
			Set(_isOn);
		}
	}

	public abstract Color Color { get; }

	public abstract Texture Texture { get; }

	void Awake()
	{
		// Initialize renderer
		_renderer = GetComponent<MeshRenderer>();
		_renderer.material.SetTexture("_EmissionMap", Texture);

		// Off
		_off = new MaterialPropertyBlock();
		_off.SetColor("_EmissionColor", Color * OffStrength);

		// On
		_on = new MaterialPropertyBlock();
		_on.SetColor("_EmissionColor", Color * OnStrength);

		// Init state
		IsOn = false;
	}

	public void Set(bool on, bool stopBlinking = true)
	{
		if (stopBlinking)
			StopBlink();

		_renderer.SetPropertyBlock(on ? _on : _off);
		SetMinimapActive(on);
	}

	public TrafficLightBulbBehaviour StartBlink()
	{
		if (_blinking == null)
			_blinking = StartCoroutine(Blink());

		return this;
	}

	public TrafficLightBulbBehaviour StopBlink()
	{
		if (_blinking != null)
			StopCoroutine(_blinking);

		_blinking = null;
		return this;
	}

	IEnumerator Blink()
	{
		for (var isOn = IsOn;; isOn = !isOn)
		{
			Set(isOn, false);
			yield return new WaitForSeconds(0.5f);
		}
	}



	static GameObject _minimapResource;
	GameObject _minimapObject;
	void SetMinimapActive(bool on)
	{
		if (_minimapObject == null)
		{
			if (_minimapResource == null)
				_minimapResource = Resources.Load<GameObject>("MinimapIcon");
			
			_minimapObject = Instantiate(_minimapResource, transform);

			var material = _minimapObject.GetComponent<Renderer>().material;
			material.color = Color;
			material.SetColor("_TintColor", Color);

			if (Texture != null)
				material.mainTexture = Texture;
		}

		_minimapObject.SetActive(on);
	}
}
