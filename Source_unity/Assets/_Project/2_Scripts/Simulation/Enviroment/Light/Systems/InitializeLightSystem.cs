﻿using System.Collections.Generic;
using System.Linq;
using Entitas;

public class InitializeLightSystem : ReactiveSystem<CoreEntity>
{
	/// <inheritdoc />
	public InitializeLightSystem(IContext<CoreEntity> context) : base(context)
	{
	}

	/// <inheritdoc />
	protected override ICollector<CoreEntity> GetTrigger(
		IContext<CoreEntity> context)
	{
		return context
			.GetGroup(
				CoreMatcher.AllOf(
					CoreMatcher.LightState,
					CoreMatcher.Team))
			.CreateCollector();
	}

	/// <inheritdoc />
	protected override bool Filter(CoreEntity entity)
	{
		return !entity.hasLight;
	}

	/// <inheritdoc />
	protected override void Execute(List<CoreEntity> entities)
	{
		for (var i = entities.Count - 1; i >= 0; i--)
		{
			var id = entities[i].lightState.Id;
			var lights = Level.Lights
				.Where(light => light.Id == id)
				.ToArray();

			entities[i].AddLight(lights);
		}
	}
}
