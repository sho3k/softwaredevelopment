﻿using System.Collections.Generic;
using Entitas;

public class LightSystem : ReactiveSystem<CoreEntity>
{
	static readonly IAllOfMatcher<CoreEntity> Matcher = CoreMatcher.AllOf(
		CoreMatcher.LightState,
		CoreMatcher.Light,
		CoreMatcher.Team);

	readonly IGroup<CoreEntity> _lights;

	/// <inheritdoc />
	public LightSystem(IContext<CoreEntity> context) : base(context)
	{
		_lights = context.GetGroup(
			Matcher);
	}

	/// <inheritdoc />
	protected override ICollector<CoreEntity> GetTrigger(
		IContext<CoreEntity> context)
	{
		return context
			.GetGroup(Matcher)
			.CreateCollector();
	}

	/// <inheritdoc />
	protected override bool Filter(CoreEntity entity)
	{
		return entity.isEnabled
			&& entity.team.Active;
	}

	/// <inheritdoc />
	protected override void Execute(List<CoreEntity> entities)
	{
		var lights = _lights.GetEntities();

		for (var i = entities.Count - 1; i >= 0; i--)
		{
			var entity = entities[i];
			if (entity.hasLightState == false)
				continue;

			var state = entity.lightState;
			foreach (var light in entity.light.Lights)
			{
				light.SetState(state.Status);
				light.SetTime(state.Time);
			}

			for (var j = 0; j < lights.Length; j++)
			{
				if (lights[j] != entity &&
					lights[j].isEnabled &&
					lights[j].team.Entity == entity.team.Entity &&
					lights[j].lightState.Id == entity.lightState.Id)
				{
					lights[j].Destroy();
					break;
				}
			}
		}
	}
}
