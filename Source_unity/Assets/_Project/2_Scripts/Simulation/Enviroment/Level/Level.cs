﻿using UnityEngine;

public class Level : MonoBehaviour
{
	public static Node[] Nodes { get; private set; }
	public static SpawnNode[] SpawnNodes { get; private set; }
	public static TrafficLightBehaviour[] Lights { get; private set; }

	void Awake()
	{
		var level = FindObjectOfType<Level>();
		if (level != this)
		{
			Debug.LogError("Multiple instances of Level found!");
			Destroy(gameObject);
			return;
		}
		
		Nodes = FindObjectsOfType<Node>();
		SpawnNodes = FindObjectsOfType<SpawnNode>();
		Lights = FindObjectsOfType<TrafficLightBehaviour>();
	}
}
