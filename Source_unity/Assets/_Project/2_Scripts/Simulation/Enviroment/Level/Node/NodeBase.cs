﻿using System;
using UnityEngine;

public class NodeBase : MonoBehaviour
{
	[NonSerialized]
	public Node Node;

	void Awake()
	{
		Node = GetComponent<Node>();
	}
}