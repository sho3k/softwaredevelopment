﻿using UnityEngine;

[ExecuteInEditMode]
public class NodeVisualizer : MonoBehaviour
{
	public static float Size = 0.1f;
	public static bool DrawLines = true;
	static Material _material;
	Color? _color;

	void Start()
	{
		_color = GetColor(this);
	}

	void OnRenderObject()
	{
		var component = GetComponent<Node>();
		if (component == null || component.Nodes == null || !DrawLines)
			return;

		if (_material == null)
			_material = new Material(Shader.Find("Custom/DebugLine"));

		_material.SetPass(0);
		GL.Begin(GL.QUADS);

		var camPos = Camera.current.transform.position;
		var position = transform.position;
		var deltaCam = position - camPos;
		var col = _color ?? GetColor(this);

		foreach (var node in component.Nodes)
		{
			if (node == null)
				continue;

			var target = node.transform.position;
			var delta = position - target;
			var dir = Vector3.Cross(delta, deltaCam);
			dir = dir.normalized * Size;

			GL.Color(col);
			GL.Vertex(position + dir);
			GL.Vertex(position - dir);

			GL.Color(GetColor(node));
			GL.Vertex(target - dir);
			GL.Vertex(target + dir);
		}

		GL.End();
	}

	static Color GetColor(Component node)
	{
		var color = new Color(1, 1, 1, 1);

		if (node.GetComponent<SpawnNode>() ||
			node.GetComponent<DestinationNode>())
			return color;
		
		if (node.GetComponent<CarNode>())
			color.r *= 0.3f;

		if (node.GetComponent<BusNode>())
			color.g *= 0.3f;

		if (node.GetComponent<PedestrianNode>())
			color.b *= 0.3f;

		if (node.GetComponent<BikeNode>())
			color.g = color.b = 0.3f;

		return color;
	}
}
