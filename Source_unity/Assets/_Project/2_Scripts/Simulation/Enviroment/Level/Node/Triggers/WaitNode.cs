﻿using System;
using UnityEngine;

public class WaitNode : NodeBase
{
	public int LightId;
	public int RequiredState = 2;

	public float VehicleDetectorStart;
	
	public static WaitNode FindInPath(CoreEntity entity, out float distance)
	{
		var position = entity.position.Value;
		var previous = entity.target.Target;
		distance = Vector3.Distance(position, previous.Position);

		if (previous.Wait != null)
			return previous.Wait;

		foreach (var node in entity.path.Path)
		{
			var wait = node.Wait;
			if (wait != null)
				return wait;

			var wpos = node.Position;
			position = previous.Position;
			previous = node;

			distance += Vector3.Distance(position, wpos);
		}

		return null;
	}
}
