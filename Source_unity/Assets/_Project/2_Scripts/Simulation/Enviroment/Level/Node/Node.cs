﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
	/// <summary>
	///     Forward connecting nodes
	/// </summary>
	public Node[] Nodes;

	/// <summary>
	///     World position of the node's tranform
	/// </summary>
	[NonSerialized] public Vector3 Position;
	
	[NonSerialized] public BikeNode Bike;
	[NonSerialized] public BusNode Bus;
	[NonSerialized] public CarNode Car;
	[NonSerialized] public PedestrianNode Pedestrian;
	[NonSerialized] public TrainNode Train;

	[NonSerialized] public DestinationNode Destination;
	[NonSerialized] public SpawnNode Spawn;
	[NonSerialized] public WaitNode Wait;

	void Awake()
	{
		Position = transform.position;

		Bus = GetComponent<BusNode>();
		Bike = GetComponent<BikeNode>();
		Car = GetComponent<CarNode>();
		Pedestrian = GetComponent<PedestrianNode>();
		Train = GetComponent<TrainNode>();

		Destination = GetComponent<DestinationNode>();
		Spawn = GetComponent<SpawnNode>();
		Wait = GetComponent<WaitNode>();
	}

	/// <summary>
	///     Find all nodes with the defined component,
	///     that are connected
	/// </summary>
	/// <remarks>
	///     Search includes itsself
	/// </remarks>
	public List<Node> FindAll(Predicate<Node> predicate)
	{
		return FindAll(
			predicate,
			new List<Node>(),
			new HashSet<Node>());
	}

	public List<Node> FindAll(
		Predicate<Node> predicate,
		List<Node> cache,
		HashSet<Node> visited)
	{
		if (visited.Add(this) == false)
			return cache;

		if (predicate(this))
			cache.Add(this);

		foreach (var node in Nodes)
			node.FindAll(predicate, cache, visited);

		return cache;
	}
}
