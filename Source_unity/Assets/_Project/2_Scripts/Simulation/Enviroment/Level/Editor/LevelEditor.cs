﻿using System;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Level))]
public class LevelEditor : Editor
{
	int control = "LevelEditor".GetHashCode();
	Node _selected;
	string _streetName;
	int[] _lightIds;
	TrafficLightBehaviour[] _lights;
	static readonly string[] StateIds =
	{
		"0",
		"1",
		"2",
		"3",
		"4",
		"5",
		"6",
		"7",
		"8",
		"9"
	};

	void OnEnable()
	{
		_lights = FindObjectsOfType<TrafficLightBehaviour>();
		_lightIds = _lights.Select(x => x.Id)
			.Distinct()
			.ToArray();
	}

	void OnSceneGUI()
	{
		if (_selected == null)
			return;

		const float size = 1.0f;
		var transform = _selected.transform;
		var position = transform.position;

		Handles.color = Color.red;

		EditorGUI.BeginChangeCheck();
		position = Handles.Slider2D(
			position,
			Vector3.up,
			Vector3.right,
			Vector3.forward,
			size,
			Handles.CircleHandleCap,
			Vector2.zero);

		if (EditorGUI.EndChangeCheck())
		{
			Undo.RecordObject(transform, "Change Node");
			transform.position = position;
			var traobj = new SerializedObject(transform);
			traobj.SetIsDifferentCacheDirty();
			traobj.Update();
		}
		
		var isBike = _selected.GetComponent<BikeNode>() != null;
		var isCar = _selected.GetComponent<CarNode>() != null;
		var isBus = _selected.GetComponent<BusNode>() != null;
		var ispedestrian = _selected.GetComponent<PedestrianNode>() != null;

		var lights = _lights.Where(x =>
		{
			var id = x.Id / 100;
			return isBike && id == BikeNode.Id
				|| isCar && id == CarNode.Id
				|| isBus && id == BusNode.Id
				|| ispedestrian && id == PedestrianNode.Id
				|| id == TrainNode.CrossingId;
		});

		var style = new GUIStyle("Button")
		{
			fontSize = 10,
			fontStyle = FontStyle.Bold,
			margin = new RectOffset(0, 0, 0, 0),
			padding = new RectOffset(0, 0, 0, 0),
		};


		
		if (Event.current.alt)
		{
			var wait = _selected.GetComponent<WaitNode>();
			var waitId = wait == null
				? -1
				: wait.LightId;

			var camPos = Camera.current.transform.position;
			foreach (var light in lights)
			{
				var selected = light.Id == waitId;
				var pos = light.transform.position;
				var rot = Quaternion.LookRotation(camPos - pos);

				Handles.color = selected ? Color.blue : Color.white;
				Handles.Label(pos, light.Id.ToString(), style);
				if (Handles.Button(pos, rot, 0.4f, 0.8f, Handles.RectangleHandleCap))
				{
					SetNodeWait(_selected, selected ? -1 : light.Id);
				}
			}
		}
		


		var nodes = FindObjectsOfType<Node>();
		var obj = new SerializedObject(_selected);
		var pro = obj.FindProperty("Nodes");
		var up = Quaternion.LookRotation(Vector3.up);

		for (int i = pro.arraySize - 1; i >= 0; i--)
		{
			if (pro.GetArrayElementAtIndex(i).objectReferenceValue == null)
				pro.DeleteArrayElementAtIndex(i);
		}

		obj.ApplyModifiedPropertiesWithoutUndo();


		foreach (var node in nodes)
		{
			if (node == _selected)
				continue;

			var pos = node.transform.position;
			var index = -1;

			for (var i = 0; i < pro.arraySize; i++)
			{
				if (pro.GetArrayElementAtIndex(i).objectReferenceValue == node)
				{
					index = i;
					break;
				}
			}

			Handles.color = index < 0
				? Color.white
				: Color.blue;

			if (Handles.Button(pos, up, 0.4f, 0.8f, Handles.CircleHandleCap))
			{
				if (Event.current.shift)
				{
					if (index < 0)
					{
						pro.InsertArrayElementAtIndex(0);
						pro.GetArrayElementAtIndex(0).objectReferenceValue = node;
					}
					else
					{
						pro.DeleteArrayElementAtIndex(index);
					}

					obj.ApplyModifiedProperties();
					obj.Update();
					Repaint();
				}
				else
				{
					_selected = node;
					Repaint();
				}
			}
		}

		if (Event.current.control && Event.current.button == 0 && Event.current.type == EventType.mouseDown)
		{
			GUIUtility.hotControl = control;

			float enter;
			var plane = new Plane(Vector3.up, new Vector3(0, position.y, 0));
			var ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
			if (plane.Raycast(ray, out enter))
			{
				var newNode = CreateNewNode(_selected.transform.parent);
				newNode.transform.position = ray.GetPoint(enter);
			}

			Event.current.Use();
			Repaint();
		}
	}

	/// <inheritdoc />
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		var level = (Level)target;
		foreach (Transform street in level.transform)
		{
			var streetObject = street.gameObject;
			EditorGUILayout.BeginVertical();
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button(streetObject.activeSelf ? "-" : "O"))
			{
				Undo.RegisterCompleteObjectUndo(streetObject, "Enabled");
				streetObject.SetActive(!streetObject.activeSelf);
			}

			GUILayout.Label(street.name);
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("/\\"))
			{
				Undo.RegisterCompleteObjectUndo(level.transform, "Reparent");
				street.SetSiblingIndex(street.GetSiblingIndex() - 1);
			}
			if (GUILayout.Button("\\/"))
			{
				Undo.RegisterCompleteObjectUndo(level.transform, "Reparent");
				street.SetSiblingIndex(street.GetSiblingIndex() + 1);
			}
			if (GUILayout.Button("X"))
			{
				Undo.DestroyObjectImmediate(streetObject);
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.EndVertical();
				Repaint();
				return;
			}
			EditorGUILayout.EndHorizontal();

			if (!streetObject.activeSelf)
			{
				EditorGUILayout.EndVertical();
				continue;
			}

			EditorGUI.indentLevel++;
			foreach (Transform child in street)
			{
				var obj = child.gameObject;
				var node = obj.GetComponent<Node>();
				if (node == null)
					continue;

				EditorGUI.BeginChangeCheck();
				EditorGUILayout.BeginHorizontal();
				var selected = GUILayout.Toggle(node == _selected, "");
				if (node == _selected != selected)
				{
					_selected = selected
						? node
						: null;
				}

				Func<Type, string, bool> check = (type, s) =>
				{
					var comp = child.GetComponent(type);
					var change = GUILayout.Toggle(comp != null, s, "Button");

					if (comp != change)
					{
						if (comp) Undo.DestroyObjectImmediate(comp);
						else Undo.AddComponent(obj, type);
					}

					return change;
				};
				
				var isBike = check(typeof(BikeNode), "Bike");
				var isCar = check(typeof(CarNode), "Car");
				var isBus = check(typeof(BusNode), "Bus");
				var ispedestrian = check(typeof(PedestrianNode), "Pedestrian");

				var availableLights = _lightIds.Where(x =>
						isBike && x / 100 == BikeNode.Id
						|| isCar && x / 100 == CarNode.Id
						|| isBus && x / 100 == BusNode.Id
						|| ispedestrian && x / 100 == PedestrianNode.Id
						|| x / 100 == TrainNode.CrossingId)
						.ToList();
				availableLights.Sort();


				EditorGUILayout.Separator();
				check(typeof(SpawnNode), "Spawn");
				check(typeof(DestinationNode), "Destination");

				
				var wait = node.GetComponent<WaitNode>();
				var index = wait == null
					? -1
					: availableLights.IndexOf(wait.LightId);

				var newIndex = EditorGUILayout.Popup(
					index,
					availableLights.Select(x => x.ToString()).ToArray());

				if (index != newIndex)
				{
					if (newIndex < 0)
					{
						Undo.DestroyObjectImmediate(wait);
					}
					else
					{
						if (wait == null)
						{
							wait = obj.AddComponent<WaitNode>();
							wait.LightId = availableLights[newIndex];
							Undo.RegisterCreatedObjectUndo(wait, "New State");
						}
						else
						{
							Undo.RegisterCompleteObjectUndo(wait, "New State");
							wait.LightId = availableLights[newIndex];
						}
					}

					wait = node.GetComponent<WaitNode>();
				}
				
				WithEnable(wait != null, () =>
				{
					var state = wait?.RequiredState ?? -1;
					var newState = EditorGUILayout.Popup(state, StateIds);

					if (state != newState)
						wait.RequiredState = newState;
				});

				if (EditorGUI.EndChangeCheck())
					ConstructNodeName(child);

				if (GUILayout.Button("X"))
				{
					Undo.DestroyObjectImmediate(obj);
					Repaint();
					EditorGUILayout.EndHorizontal();
					EditorGUI.indentLevel--;
					EditorGUILayout.EndVertical();
					return;
				}

				EditorGUILayout.EndHorizontal();
			}

			if (GUILayout.Button("Add Node"))
				CreateNewNode(street);

			EditorGUI.indentLevel--;
			EditorGUILayout.EndVertical();
		}
		

		GUILayout.Space(20);
		EditorGUILayout.BeginHorizontal();
		_streetName = GUILayout.TextField(_streetName);
		if (GUILayout.Button("Add Street", GUILayout.Width(90)))
		{
			var newStreet = new GameObject(_streetName);
			newStreet.transform.SetParent(level.transform, false);
			_streetName = "";
			Undo.RegisterCreatedObjectUndo(newStreet, "New Street");
		}
		EditorGUILayout.EndHorizontal();
	}

	GameObject CreateNewNode(Transform street)
	{
		var newNode = new GameObject("Node", typeof(Node), typeof(NodeVisualizer));
		newNode.transform.SetParent(street, false);

		if (_selected != null)
			newNode.transform.position = _selected.transform.position;

		ConstructNodeName(newNode.transform);
		Undo.RegisterCreatedObjectUndo(newNode, "New Node");
		return newNode;
	}

	static void SetNodeWait(Component node, int id)
	{
		var wait = node.GetComponent<WaitNode>();

		if (id < 0)
		{
			if (wait != null)
				Undo.DestroyObjectImmediate(wait);
		}
		else
		{
			if (wait == null)
				wait = Undo.AddComponent<WaitNode>(node.gameObject);
			else Undo.RecordObject(wait, "New State");

			var obj = new SerializedObject(wait);
			var pro = obj.FindProperty("LightId");
			pro.intValue = id;
			obj.ApplyModifiedPropertiesWithoutUndo();
			obj.Update();
		}
	}


	static void WithEnable(bool enabled, Action callback)
	{
		var tmpEnabled = GUI.enabled;
		GUI.enabled = enabled;
		callback();
		GUI.enabled = tmpEnabled;
	}



	static void ConstructNodeName(Transform transform)
	{
		var builder = new StringBuilder("Node_")
			.Append(transform.parent.name);

		Action<Type, string> check = (type, s) =>
		{
			if (transform.GetComponent(type) != null)
				builder.Append(s);
		};

		check(typeof(BikeNode), "_Bike(F)");
		check(typeof(CarNode), "_Car(F)");
		check(typeof(BusNode), "_Bus(F)");
		check(typeof(PedestrianNode), "_Pedestrian(F)");
		check(typeof(TrainNode), "_Train(F)");

		check(typeof(DestinationNode), "_Destination(T)");
		check(typeof(SpawnNode), "_Spawn(T)");
		check(typeof(WaitNode), "_Wait(T)");

		transform.name = builder.ToString();
	}
}
