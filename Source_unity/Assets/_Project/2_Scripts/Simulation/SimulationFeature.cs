﻿using Entitas;

public class SimulationFeature : SystemBehaviour
{
	protected override Systems Construct(Contexts contexts)
	{
		var context = contexts.core;
		return new Feature("Simulation")
			.Add(new RequestSystem(context))
			.Add(new ResponseSystem(context))
			.Add(new CreateConnectionSystem(context))
			.Add(new StateSystem(context))
			.Add(new TimeSystem(context))
				
			.Add(new SpawnCreatorSystem(context))
			.Add(new SpawnSystem(context))

			.Add(new InitializeLightSystem(context))
			.Add(new LightSystem(context))
			
			.Add(new InitializeVehicleSystem(context))
			.Add(new GeneratePathSystem(context))
			.Add(new PolygonSystem(context))
			.Add(new AiSystem(context));
	}
}
