﻿using Entitas;
using UnityEngine;

public class TimeSystem : IExecuteSystem
{
	readonly IGroup<CoreEntity> _group;

	/// <inheritdoc />
	public TimeSystem(IContext<CoreEntity> context)
	{
		_group = context.GetGroup(
			CoreMatcher.AllOf(
				CoreMatcher.Time,
				CoreMatcher.Timedelta,
				CoreMatcher.Timescale,
				CoreMatcher.Connection));
	}

	/// <inheritdoc />
	public void Execute()
	{
		var deltaTime = Time.unscaledDeltaTime;

		foreach (var entity in _group.GetEntities())
		{
			var delta = deltaTime * entity.timescale.Scale;
			//entity.time.Time += delta;
			//entity.timedelta.Delta = delta;
			
			entity.ReplaceTime(entity.time.Time + delta);
			entity.ReplaceTimedelta(delta);
		}
	}
}
