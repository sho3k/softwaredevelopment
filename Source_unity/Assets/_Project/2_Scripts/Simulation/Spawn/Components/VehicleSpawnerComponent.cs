﻿using Entitas;

[Core]
public class VehicleSpawnerComponent : IComponent
{
	public float MinRate;
	public float MaxRate;
}