﻿using System.Collections.Generic;
using Entitas;

public class SpawnCreatorSystem : ReactiveSystem<CoreEntity>
{
	readonly IContext<CoreEntity> _context;
	
	public SpawnCreatorSystem(IContext<CoreEntity> context) : base(context)
	{
		_context = context;
	}

	protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
	{
		var group = context.GetGroup(
			CoreMatcher.Connection);

		return group.CreateCollector();
	}

	protected override bool Filter(CoreEntity entity)
	{
		return true;
	}

	protected override void Execute(List<CoreEntity> entities)
	{
		foreach (var entity in entities)
		{
			// Real values
			Create(entity, VehicleType.Car, 1.5f, 3.0f);
			Create(entity, VehicleType.Train, 1800.0f, 3600.0f);
			Create(entity, VehicleType.Bus, 100.0f, 1200.0f);
			Create(entity, VehicleType.Bike, 5.0f, 20.0f);
			Create(entity, VehicleType.Pedestrian, 3.0f, 30.0f);

			// Stress test
			/*Create(entity, VehicleType.Car, 1.0f, 3.0f);
			Create(entity, VehicleType.Train, 1800.0f, 3600.0f);
			Create(entity, VehicleType.Bus, 2.0f, 12.0f);
			Create(entity, VehicleType.Bike, 1.0f, 2.0f);
			Create(entity, VehicleType.Pedestrian, 0.5f, 1.5f);*/


			var state = Create(entity);
			state.AddLightState(502, 0, -1);

			state = Create(entity);
			state.AddLightState(601, 2, -1);
		}
	}

	CoreEntity Create(CoreEntity entity)
	{
		var creator = _context.CreateEntity();
		creator.AddTeam(entity);
		return creator;
	}

	CoreEntity Create(CoreEntity entity, VehicleType type, float min, float max)
	{
		var creator = _context.CreateEntity();
		creator.AddTeam(entity);
		creator.AddVehicleSpawner(min, max);
		creator.AddVehicle(type);
		return creator;
	}
}