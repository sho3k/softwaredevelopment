﻿using Entitas;
using UnityEngine;

public class SpawnSystem : IExecuteSystem
{
	readonly IContext<CoreEntity> _context;
	readonly IGroup<CoreEntity> _group;
	
	public SpawnSystem(IContext<CoreEntity> context)
	{
		_context = context;
		_group = _context.GetGroup(CoreMatcher.AllOf(
			CoreMatcher.VehicleSpawner,
			CoreMatcher.Vehicle,
			CoreMatcher.Team));
	}
	
	/// <inheritdoc />
	public void Execute()
	{
		foreach (var entity in _group.GetEntities())
		{
			var team = entity.team.Entity;
			if (entity.vehicleSpawner.MaxRate < 0)
				continue;

			var time = GetTime(entity);
			time -= team.timedelta.Delta;
			
			if (time < 0)
			{
				// Create vehicle
				var type = entity.vehicle.Type;
				var vehicle = _context.CreateEntity();
				vehicle.AddTeam(team);
				vehicle.AddVehicle(type);
				
				// Calculate new spawn time
				time += GetNewRate(entity);
			}
			
			entity.ReplaceTime(time);
		}
	}


	static float GetTime(CoreEntity entity)
	{
		if (entity.hasTime)
			return entity.time.Time;

		var target = GetNewRate(entity);
		entity.AddTime(target);
		return target;
	}

	static float GetNewRate(CoreEntity entity)
	{
		var rate = entity.vehicleSpawner;
		return Random.Range(rate.MinRate, rate.MaxRate);
	}
}