﻿using System;
using System.Collections.Generic;
using System.Text;
using Entitas;
using Models;
using SimpleJSON;
using UnityEngine;

public class StateSystem : ReactiveSystem<CoreEntity>
{
	readonly IContext<CoreEntity> _context;
	readonly IGroup<CoreEntity> _states;
	readonly IGroup<CoreEntity> _waiters;
	readonly Dictionary<int, Traffic> _stateCache;
	readonly List<CoreEntity> _dirtyCache;

	// Json building
	readonly JSONNull _null;
	readonly JSONArray _array;
	readonly JSONObject _storage;
	readonly JSONObject _container;

	readonly StringBuilder _jsonBuilder;

	/// <inheritdoc />
	public StateSystem(IContext<CoreEntity> context) : base(context)
	{
		_context = context;

		_waiters = context.GetGroup(
			CoreMatcher.LightWait);

		_states = context.GetGroup(
			CoreMatcher.AllOf(
				CoreMatcher.Connection,
				CoreMatcher.State));

		_stateCache = new Dictionary<int, Traffic>();
		_dirtyCache = new List<CoreEntity>(512);

		// 
		_null = new JSONNull();
		_array = new JSONArray();
		_storage = new JSONObject();
		_container = new JSONObject();
		_container.Dictionary.Add("TrafficUpdate", _storage);

		_jsonBuilder = new StringBuilder(8096);
	}

	/// <inheritdoc />
	protected override ICollector<CoreEntity> GetTrigger(
		IContext<CoreEntity> context)
	{
		var group = context.GetGroup(CoreMatcher.LightWait);

		return group.CreateCollector(GroupEvent.AddedOrRemoved);
	}

	/// <inheritdoc />
	protected override bool Filter(CoreEntity entity)
	{
		return entity.hasTeam;
	}

	/// <inheritdoc />
	protected override void Execute(List<CoreEntity> entities)
	{
		FetchDirty(entities);

		var waiters = _waiters.GetEntities();
		foreach (var entity in _dirtyCache)
		{
			DetermineState(waiters, entity);
			BuildState(entity);
		}
	}
	
	void BuildState(CoreEntity entity)
	{
		var current = entity.state.State;
		var last = current.Count - 1;

		foreach (var item in _stateCache)
		{
			var key = item.Key;
			var value = item.Value;
			for (var i = last; i >= 0; i--)
			{
				var c = current[i];
				if (c.LightId != key)
					continue;

				if (c.Equals(value))
				{
					current[i] = current[last];
					current[last] = c;
					value.Dispose();
				}
				else
				{
					current[i].Dispose();
					current[i] = current[last];
					current[last] = value;
					SendTraffic(value, entity);
				}
				
				last--;
				key = -1;
				break;
			}

			if (key >= 0)
			{
				current.Add(value);
				SendTraffic(value, entity);
			}
		}

		for (var index = last; index >= 0; index--)
		{
			var c = current[index];
			if (c.Count == 0 && c.DirectionRequests.Count == 0)
				continue;

			c.Count = 0;
			c.DirectionRequests.Clear();
			SendTraffic(c, entity);
		}
	}

	void SendTraffic(Traffic traffic, CoreEntity entity)
	{
		traffic.UpdateJson(_storage, _array, _null);

		_jsonBuilder.Length = 0;
		_container.WriteToStringBuilder(_jsonBuilder);

		var request = _context.CreateEntity();
		request.AddTeam(entity);
		request.AddRequest(_jsonBuilder);
	}

	void DetermineState(IReadOnlyList<CoreEntity> waiters, CoreEntity entity)
	{
		// Determine state
		//foreach (var traffic in _stateCache)
		//	traffic.Value.Dispose();
		_stateCache.Clear();

		var team = entity.connection.Team;
		for (var i = 0; i < waiters.Count; i++)
		{
			var waiter = waiters[i];
			if (waiter.team.Team != team)
				continue;
			
			var wait = waiter.lightWait.LightId;
			switch (wait.LightId)
			{
				case 601: continue;
			}
			
			Traffic traffic;
			if (!_stateCache.TryGetValue(wait.LightId, out traffic))
			{
				traffic = Traffic.CreateInstance(wait.LightId);
				_stateCache.Add(wait.LightId, traffic);
			}

			traffic.Count++;
			var node = wait.Node;
			if (node.Bus != null && node.Car == null)
				traffic.DirectionRequests.Add(wait.RequiredState);
		}
	}

	void FetchDirty(List<CoreEntity> entities)
	{
		var stateEntities = _states.GetEntities();
		_dirtyCache.Clear();

		for (var i = 0; i < entities.Count; i++)
		{
			var entity = entities[i];
			var team = entity.team.Team;
			foreach (var stateEntity in stateEntities)
			{
				var connection = stateEntity.connection;
				if (connection.Team == team)
					_dirtyCache.Add(stateEntity);
			}
		}
	}
}
