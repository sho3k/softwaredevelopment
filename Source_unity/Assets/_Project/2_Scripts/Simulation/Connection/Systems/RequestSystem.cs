﻿using System.Collections.Generic;
using Entitas;

public class RequestSystem : ReactiveSystem<CoreEntity>
{
	/// <inheritdoc />
	public RequestSystem(IContext<CoreEntity> context) : base(context)
	{
	}

	/// <inheritdoc />
	protected override ICollector<CoreEntity> GetTrigger(
		IContext<CoreEntity> context)
	{
		var group = context.GetGroup(
			CoreMatcher.AllOf(
				CoreMatcher.Team,
				CoreMatcher.Request));

		return group.CreateCollector();
	}

	/// <inheritdoc />
	protected override bool Filter(CoreEntity entity)
	{
		return true;
	}

	/// <inheritdoc />
	protected override void Execute(List<CoreEntity> entities)
	{
		foreach (var entity in entities)
		{
			var connection = entity.team.Connection;
			connection.Emit(entity.request.Data);
			entity.Destroy();
		}
	}
}
