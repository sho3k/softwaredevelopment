﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entitas;
using SimpleJSON;
using UnityEngine;

public class ResponseSystem : IExecuteSystem
{
	readonly IContext<CoreEntity> _context;
	readonly IGroup<CoreEntity> _connections;
	readonly IGroup<CoreEntity> _lights;

	public ResponseSystem(IContext<CoreEntity> context)
	{
		_context = context;

		_connections = context.GetGroup(
			CoreMatcher.Connection);

		_lights = context.GetGroup(
			CoreMatcher.AllOf(
				CoreMatcher.LightState,
				CoreMatcher.Team));
	}

	public void Execute()
	{
		foreach (var entity in _connections.GetEntities())
		{
			var lights = _lights.GetEntities();
			var connection = entity.connection;

			foreach (var delivery in connection.Dispatcher)
			{
				try
				{
					var response = delivery.Utf8();
					ConsoleBehaviour.Log($"Response: \"{response}\"");
					ProcessState(response, entity, lights);
				}
				catch (Exception e)
				{
					Debug.LogError(e);
					Debug.LogError(e.StackTrace);
				}
			}
		}
	}
	
	void ProcessState(string response, CoreEntity entity, CoreEntity[] lights)
	{
		var json = JSONNode.Parse(response);
		if (json == null)
		{
			Debug.LogError(response);
			return;
		}

		// Speed
		var speed = json["Speed"]?.AsFloat;
		if (speed.HasValue)
			entity.ReplaceTimescale(speed.Value);
		
		// Light state
		foreach (var lightState in FromJson(json))
		{
			var success = false;
			foreach (var light in lights)
			{
				if (light.team.Entity != entity)
					continue;

				if (light.lightState.Id == lightState.Id)
				{
					light.ReplaceLightState(
						lightState.Id,
						lightState.Status,
						lightState.Time);

					success = true;
					break;
				}
			}

			if (!success)
			{
				var e = _context.CreateEntity();
				e.AddLightState(
					lightState.Id,
					lightState.Status,
					lightState.Time);

				e.AddTeam(entity);
			}
		}
	}

	static List<LightStateComponent> FromJson(JSONNode json)
	{
		var lights = json["Lights"]?.AsArray;
		return lights?.Children.Select(
			light => new LightStateComponent
			{
				Id = light?["Id"]?.AsInt ?? -1,
				Status = light?["Status"]?.AsInt ?? -1,
				Time = light?["Time"]?.AsInt ?? -1,
			}).ToList() ?? new List<LightStateComponent>(0);
	}
}
