﻿using System.Collections.Generic;
using Entitas;
using Models;

public class CreateConnectionSystem : ReactiveSystem<CoreEntity>
{
	readonly IContext<CoreEntity> _context;

	/// <inheritdoc />
	public CreateConnectionSystem(IContext<CoreEntity> context) : base(context)
	{
		_context = context;
	}

	/// <inheritdoc />
	protected override ICollector<CoreEntity> GetTrigger(
		IContext<CoreEntity> context)
	{
		var group = context.GetGroup(
			CoreMatcher.CreateConnection);

		return group.CreateCollector();
	}

	/// <inheritdoc />
	protected override bool Filter(CoreEntity entity)
	{
		return entity.isEnabled;
	}

	/// <inheritdoc />
	protected override void Execute(List<CoreEntity> entities)
	{
		foreach (var entity in entities)
		{
			var settings = entity.createConnection.Settings;
			if (Game.SetTeam(settings.team))
				continue;
			
			var e = _context.CreateEntity();

			e.AddTime(0);
			e.AddTimedelta(0);
			e.AddTimescale(1);

			var dispatcher = new Dispatcher<Response>();
			e.AddConnection(
				settings.Connect(dispatcher),
				dispatcher);
			e.AddState(new List<Traffic>(0));

			Game.SetTeam(e.connection.Team);

			entity.Destroy();
		}
	}
}
