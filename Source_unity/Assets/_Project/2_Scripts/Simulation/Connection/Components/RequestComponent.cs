﻿using System.Text;
using Entitas;

[Core]
public class RequestComponent : IComponent
{
	public byte[] Data;
}

public static class RequestComponentHelper
{
	public static void AddRequest(this CoreEntity entity, StringBuilder builder)
	{
		var json = builder.ToString();
		var bytes = json.Bytes();
		entity.AddRequest(bytes);

		ConsoleBehaviour.Log(json);
	}

	public static void AddRequest(this CoreEntity entity, string json)
	{
		var bytes = json.Bytes();
		entity.AddRequest(bytes);

		ConsoleBehaviour.Log(json);
	}
}
