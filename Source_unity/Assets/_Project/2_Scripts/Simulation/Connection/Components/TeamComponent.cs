﻿using Entitas;

[Core]
public class TeamComponent : IComponent
{
	public CoreEntity Entity;

	public int Team => Entity.connection.Team;

	public bool Active => Entity.connection.Active;

	public Connection Connection => Entity.connection.Connection;
}
