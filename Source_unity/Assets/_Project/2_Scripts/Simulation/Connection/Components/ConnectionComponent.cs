﻿using Entitas;

[Core]
public class ConnectionComponent : IComponent
{
	public Connection Connection;
	public Dispatcher<Response> Dispatcher;

	public int Team => Connection.Host.team;

	public bool Active => Connection.Host.team == Game.Team;
}
