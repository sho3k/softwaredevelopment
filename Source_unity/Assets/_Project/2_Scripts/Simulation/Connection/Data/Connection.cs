﻿using System;
using System.Collections.Generic;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

public abstract class Connection
{
	public const int Team = 2;
	public const string SimulatorQueueName = "simulator";
	public const string ControllerQueueName = "controller";
	public const string Exchange = "";

	public readonly Settings Host;

	Connection(Settings host)
	{
		Host = host;
	}
	
	public abstract void Dispose();

	/// <summary>
	///     Send data to the server
	/// </summary>
	/// <param name="content"></param>
	public abstract void Emit(byte[] content);

	/// <summary>
	///     Implementation of connection
	/// </summary>
	sealed class GameConnection : Connection
	{
		readonly IModel _channel;
		readonly IConnection _connection;
		readonly EventingBasicConsumer _consumer;
		readonly Dispatcher<Response> _dispatcher;

		public GameConnection(Settings host, Dispatcher<Response> dispatcher)
			: base(host)
		{
			ConsoleBehaviour.Log($"Connecting with team: {Host.team}");
			
			_connection = new ConnectionFactory
			{
				HostName = Host.host,
				VirtualHost = $"/{Host.team}",
				UserName = Host.username,
				Password = Host.password,
				Port = Host.port,
			}.CreateConnection();

			// Init channel
			_channel = _connection.CreateModel();
			_channel.QueueDeclare(
				queue: SimulatorQueueName,
				durable: false,
				exclusive: false,
				autoDelete: true,
				arguments: new Dictionary<string, object>{{"x-message-ttl", 10000}});

			// Finalize
			_consumer = new EventingBasicConsumer(_channel);
			_consumer.Received += OnReceive;
			_channel.BasicConsume(SimulatorQueueName, false, _consumer);
			_dispatcher = dispatcher;
		}

		/// <inheritdoc />
		public override void Dispose()
		{
			ConsoleBehaviour.Log($"Disconnecting connection with team: {Host.team}");

			_consumer?.OnCancel();
			_channel?.Dispose();
			_connection?.Dispose();
		}

		void OnReceive(object sender, BasicDeliverEventArgs args)
		{
			_dispatcher.Push(new Response(this, args.Body));
		}

		/// <inheritdoc />
		public override void Emit(byte[] content)
		{
			var properties = _channel.CreateBasicProperties();
			_channel.BasicPublish(
				Exchange,
				ControllerQueueName,
				false,
				properties,
				content);
		}
	}

	sealed class StubConnection : Connection
	{
		readonly Dispatcher<Response> _dispatcher;

		/// <inheritdoc />
		public StubConnection(Settings host, Dispatcher<Response> dispatcher)
			: base(host)
		{
			_dispatcher = dispatcher;
		}

		/// <inheritdoc />
		public override void Dispose()
		{
		}

		/// <inheritdoc />
		public override void Emit(byte[] content)
		{
		}

		/// <summary>
		///     Stub send
		/// </summary>
		/// <param name="body"></param>
		public void Send(byte[] body)
		{
			_dispatcher.Push(new Response(this, body));
		}
	}


	[Serializable]
	public struct Settings
	{
		public string host;
		public int team;
		public int port;
		public string username;
		public string password;

		public Settings(Settings settings) : this()
		{
			host = settings.host;
			team = settings.team;
			port = settings.port;
			username = settings.username;
			password = settings.password;
		}

		public static Settings Default => new Settings
		{
			host = "localhost",
			team = Team,
			port = 5672,
			username = "guest",
			password = "guest"
		};

		public Connection Connect(Dispatcher<Response> dispatcher)
		{
			return new GameConnection(this, dispatcher);
		}

		public Connection StubConnect(
			Dispatcher<Response> dispatcher,
			out Action<byte[]> send)
		{
			var connection = new StubConnection(this, dispatcher);
			send = connection.Send;
			return connection;
		}
	}
}
