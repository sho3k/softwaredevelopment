﻿using System;
using System.Collections.Generic;
using SimpleJSON;

namespace Models
{
	[Serializable]
	public class Traffic : IDisposable, IComparable<Traffic>, IEquatable<Traffic>
	{
		static readonly Stack<Traffic> Pool = new Stack<Traffic>(512);

		public static Traffic CreateInstance(int id)
		{
			var item = Pool.Count > 0
				? Pool.Pop()
				: new Traffic();

			item.LightId = id;
			return item;
		}
		
		public int LightId;
		public int Count;
		public List<int> DirectionRequests;

		public Traffic()
		{
			DirectionRequests = new List<int>(12);
		}

		public void UpdateJson(JSONObject json, JSONArray array, JSONNull n)
		{
			if (LightId / 100 == BusNode.Id)
			{
				array.Clear();

				for (var i = 0; i < DirectionRequests.Count; i++)
				{
					var num = JSONNumber.CreateInstance(DirectionRequests[i]);
					array.Add(num);
				}

				json["DirectionRequests"] = array;
			}
			else
			{
				json["DirectionRequests"] = n;
			}
			
			json["Count"] = Count;
			json["LightId"] = LightId;
		}

		/// <inheritdoc />
		public int CompareTo(Traffic other)
		{
			return LightId.CompareTo(other.LightId);
		}

		/// <inheritdoc />
		public bool Equals(Traffic other)
		{
			if (ReferenceEquals(this, other))
				return true;

			if (other == null
				|| LightId != other.LightId
				|| Count != other.Count)
				return false;

			var requests = other.DirectionRequests;
			if (requests == DirectionRequests)
				return true;

			if (requests == null || DirectionRequests == null)
				return false;

			var length = requests.Count;
			if (DirectionRequests.Count != length)
				return false;
			
			for (var i = 0; i < length; i++)
			{
				if (DirectionRequests[i] != requests[i])
					return false;
			}

			return true;
		}

		/// <inheritdoc />
		public void Dispose()
		{
			DirectionRequests.Clear();
			Count = 0;

			Pool.Push(this);
		}
	}
}
