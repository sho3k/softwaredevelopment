﻿using System.Collections;
using System.Collections.Generic;

public class Dispatcher<T> : IEnumerable<T>
{
	readonly object _lock;
	readonly Stack<T> _received;
	readonly Stack<T> _dispatcked;

	public Dispatcher()
	{
		_lock = new object();
		_received = new Stack<T>();
		_dispatcked = new Stack<T>();
	}

	public IEnumerator<T> GetEnumerator()
	{
		lock (_lock)
		{
			while (_received.Count > 0)
				_dispatcked.Push(_received.Pop());

			_received.Clear();
		}

		while (_dispatcked.Count > 0)
			yield return _dispatcked.Pop();

		_dispatcked.Clear();
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return GetEnumerator();
	}

	public void Push(T item)
	{
		lock (_lock)
			_received.Push(item);
	}
}
