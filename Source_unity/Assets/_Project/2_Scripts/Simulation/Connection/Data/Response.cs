﻿public struct Response
{
	public readonly Connection Connection;
	public readonly byte[] Content;

	public Response(Connection connection, byte[] content)
	{
		Connection = connection;
		Content = content;
	}

	public string Utf8()
	{
		return Content.Utf8();
	}
}