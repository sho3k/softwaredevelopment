﻿using UnityEngine;

public class CarBehaviour : MonoBehaviour
{
	public MotorComponent Motor;
	public SizeComponent Size;
	public VehicleComponent Vehicle;
	public SteerLockComponent Steer;
}
