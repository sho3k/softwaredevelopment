﻿using System;
using Entitas;

[Core, Serializable]
public struct SizeComponent : IComponent
{
	public float Width;
	public float Height;
}
