﻿using System;
using Entitas;

[Core, Serializable]
public struct MotorComponent : IComponent
{
	public float Acceleration;
	public float Deceleration;
	public float MaxSpeed;
}
