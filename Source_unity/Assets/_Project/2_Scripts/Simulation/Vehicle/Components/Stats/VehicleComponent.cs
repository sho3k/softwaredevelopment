﻿using System;
using Entitas;

[Core, Serializable]
public struct VehicleComponent : IComponent
{
	public VehicleType Type;
}
