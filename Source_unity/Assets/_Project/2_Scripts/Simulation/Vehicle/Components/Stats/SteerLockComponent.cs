﻿using System;
using Entitas;

[Core, Serializable]
public struct SteerLockComponent : IComponent
{
	public float MaxAngle;
}
