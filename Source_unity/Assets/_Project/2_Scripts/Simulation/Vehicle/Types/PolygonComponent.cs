﻿using System;
using Entitas;
using UnityEngine;

[Core]
public class PolygonComponent : IComponent
{
	public Vector2[] Vertices;

	public Vector2 this[int index]
	{
		get { return Vertices[index]; }
		set { Vertices[index] = value; }
	}

	public bool Raycast(Ray2D ray, out float distance)
	{
		distance = 0;
		var length = Vertices.Length;
		var hit = false;
		for (var i = 0; i < length; i++)
		{
			var j = i + 1;
			if (j == length)
				j = 0;


			var v1 = ray.origin - Vertices[i];
			var v2 = Vertices[j] - Vertices[i];

			var v3 = ray.direction;
			v3 = new Vector2(-v3.y, v3.x);
			
			var dot = Vector2.Dot(v2, v3);
			if (Math.Abs(dot) < 0.001f)
				continue;

			var d = Cross(v2, v1) / dot;
			if (d < 0)
				continue;
			
			var t2 = Vector2.Dot(v1, v3) / dot;
			if (t2 >= 0.0 && t2 <= 1.0)
			{
				distance = hit
					? Mathf.Min(d, distance)
					: d;

				hit = true;
			}
		}

		return hit;
	}

	static float Cross(Vector2 a, Vector2 b)
	{
		return a.x * b.y - a.y * b.x;
	}
}
