﻿using System;
using System.Diagnostics;
using Entitas;
using UnityEngine;
using Object = UnityEngine.Object;

public class AiSystem : IExecuteSystem
{
	public static readonly IAllOfMatcher<CoreEntity> AiMatcher;
	static GameObject _collision;

	const float MaxTimeStep = 0.2f;
	const long MaxComputeTime = 1000 / 2;

	readonly Stopwatch _stopwatch;
	readonly IGroup<CoreEntity> _group;
	readonly IGroup<CoreEntity> _lights;

	static AiSystem()
	{
		AiMatcher = CoreMatcher.AllOf(
			CoreMatcher.Path,
			CoreMatcher.Motor,
			CoreMatcher.Target,
			CoreMatcher.Position,
			CoreMatcher.Ai,
			CoreMatcher.Team);
	}
	
	public AiSystem(IContext<CoreEntity> context)
	{
		_stopwatch = new Stopwatch();
		_group = context.GetGroup(AiMatcher);

		if (_collision == null)
			_collision = Resources.Load<GameObject>("Collision");

		_lights = context.GetGroup(
			CoreMatcher.AllOf(
				CoreMatcher.LightState,
				CoreMatcher.Light,
				CoreMatcher.Team));
	}

	/// <inheritdoc />
	public void Execute()
	{
		var evaluate = true;
		var entities = _group.GetEntities();
		var lights = _lights.GetEntities();
		_stopwatch.Restart();
		
		for (var time = 0.0f; evaluate; time += MaxTimeStep)
		{
			if (_stopwatch.ElapsedMilliseconds > MaxComputeTime)
			{
				ConsoleBehaviour.Error("AiSystem compute time has exeeded! Aborting..");
				break;
			}
			
			evaluate = false;
			foreach (var entity in entities)
			{
				if (entity.isEnabled == false)
					continue;

				var connection = entity.team.Entity;
				var deltaTime = connection.timedelta.Delta - time;
				if (deltaTime <= 0)
					continue;

				// Exeeding max time step, evaluation has to be redone
				if (deltaTime > MaxTimeStep)
				{
					evaluate = true;
					deltaTime = MaxTimeStep;
				}

				float breakDistance;
				var wait = Wait(entity, lights, out breakDistance);
				
				var position = new Vector2(
					entity.position.Value.x,
					entity.position.Value.z);
				
				breakDistance = BreakDistance(
					entity,
					entities,
					breakDistance);

				if (entity.isEnabled == false)
					continue;

				Throttle(entity, breakDistance);
				Speed(entity);

				if (Move(entity, wait, deltaTime))
				{
					Destroy(entity);
					continue;
				}

				Rotate(entity, position);
			}
		}

		_stopwatch.Stop();
	}

	static void Destroy(CoreEntity entity)
	{
		if (entity.hasAsset)
		{
			var obj = entity.asset.GameObject;
			if (obj != null)
				Object.Destroy(obj);
		}

		entity.Destroy();
	}

	static WaitNode Wait(CoreEntity entity, CoreEntity[] lights, out float breakDistance)
	{
		var connection = entity.team.Entity;
		var wait = WaitNode.FindInPath(entity, out breakDistance);

		if (wait == null || breakDistance > wait.VehicleDetectorStart)
		{
			if (entity.hasLightWait)
				entity.RemoveLightWait();
		}
		else
		{
			if (entity.hasLightWait)
				entity.ReplaceLightWait(wait);
			else
				entity.AddLightWait(wait);
		}

		if (ValidWait(wait, connection, lights) || wait == entity.target.Target.Wait)
		{
			breakDistance = Mathf.Infinity;
			return null;
		}

		return wait;
	}

	static bool ValidWait(WaitNode node, IAERC connection, CoreEntity[] lights)
	{
		if (node == null)
			return true;

		var id = node.LightId;
		var target = node.RequiredState;
		foreach (var light in lights)
		{
			if (!light.isEnabled)
				continue;

			if (light.team.Entity != connection)
				continue;

			var state = light.lightState;
			if (state.Id != id)
				continue;

			if (id / 100 == 6)
			{
				try
				{
					var l = light.light.Lights[0] as TrafficLightTrainBehaviour;
					return state.Status == 2
						&& l.Current == TrafficLightTrainBehaviour.State.Idle;
				}
				catch (Exception e)
				{
					ConsoleBehaviour.Exception(e);
					return true;
				}
			}

			if (id == 502)
				return true;

			switch (state.Status)
			{
				case 5:
					return target == 2
						|| target == 3;

				case 6:
					return target == 2
						|| target == 4;

				case 7:
					return target == 3
						|| target == 4;

				case 8: return true;
				default: return state.Status == target;
			}
		}

		return false;
	}


	static float BreakDistance(CoreEntity entity, CoreEntity[] entities, float breakDistance)
	{
		if (!entity.hasPolygon)
			return breakDistance;

		CoreEntity collision = null;
		var ray = GetRay(entity);

		foreach (var other in entities)
		{
			if (other == entity || !other.isEnabled || !other.hasPolygon)
				continue;

			float hit;
			if (other.polygon.Raycast(ray, out hit))
			{
				hit -= 2.0f * entity.size.Height + other.size.Height;
				if (hit < breakDistance)
				{
					collision = other;
					breakDistance = hit;
				}
			}
		}


		var size = entity.size.Height * 2;
		if (collision == null || breakDistance > size)
			return breakDistance;

		ray = GetRay(collision);

		float tmp;
		if (!entity.polygon.Raycast(ray, out tmp))
			return breakDistance;

		if (tmp > size)
			return breakDistance;


		var mean = (collision.position.Value + entity.position.Value) / 2.0f;
		Object.Instantiate(_collision, mean, Quaternion.identity);

		Destroy(collision);
		Destroy(entity);
		return -1;
	}

	static Ray2D GetRay(CoreEntity entity)
	{
		var angle = entity.angle.Angle;
		var forward = new Vector2(
			Mathf.Cos(angle),
			Mathf.Sin(angle));

		var front = new Vector2(
			entity.position.Value.x + forward.x * entity.size.Height,
			entity.position.Value.z + forward.y * entity.size.Height);

		return new Ray2D(front, forward);
	}
	
	static void Throttle(CoreEntity entity, float breakDistance)
	{
		var dec = entity.motor.Deceleration;
		var vel = breakDistance > 0.01f
			? Mathf.Sqrt(dec * breakDistance)
			: 0;

		var cur = entity.speed.Speed;
		var acc = entity.motor.Acceleration;
		var del = Mathf.Min(vel - cur, acc);

		entity.ReplaceThrottle(del);
	}

	static void Speed(CoreEntity entity)
	{
		// Calculate speed
		var thr = entity.throttle.Throttle;
		var cur = entity.speed.Speed;
		var max = entity.motor.MaxSpeed;
		var speed = Mathf.Clamp(cur + thr, 0, max);

		entity.ReplaceSpeed(speed);
	}

	static bool Move(CoreEntity entity, WaitNode wait, float deltaTime)
	{
		var distance = entity.speed.Speed * deltaTime;
		for (; ; )
		{
			var target = entity.target.Target;
			var delta = target.Position - entity.position.Value;
			var magnitude = delta.magnitude;

			if (distance < magnitude)
			{
				var travel = delta * (distance / magnitude);
				entity.ReplacePosition(entity.position.Value + travel);
				break;
			}

			if (target == entity.destination.Destination)
			{
				// Debug.Log("Finished");
				entity.ReplacePosition(target.Position);
				return true;
			}

			if (wait != null && wait.Node == entity.path.Path.Peek())
			{
				break;
			}

			// Debug.Log("Next target");

			var newNode = entity.path.Path.Pop();
			entity.ReplaceTarget(newNode);
			entity.ReplacePosition(target.Position);
			distance -= magnitude;
		}

		return false;
	}

	static void Rotate(CoreEntity entity, Vector2 position)
	{
		var targetPosition = entity.target.Target.Position;

		var dlt = new Vector2(
			targetPosition.x - position.x,
			targetPosition.z - position.y);

		var mag = dlt.magnitude;

		if (mag > 0.006f)
		{
			var dir = dlt / mag;
			var ang = Mathf.Atan2(dir.x, dir.y);

			entity.ReplaceSteer(Mathf.Clamp(
				ang,
				-entity.steerLock.MaxAngle,
				entity.steerLock.MaxAngle));
				
			//entity.angle.Angle = Mathf.PI / 2 - ang;
			entity.ReplaceAngle(Mathf.PI / 2 - ang);

			var rot = Quaternion.Euler(0, ang * Mathf.Rad2Deg, 0);
			entity.ReplaceRotation(rot);
		}
	}
}
