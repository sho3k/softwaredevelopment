﻿using System.Collections.Generic;
using System.Linq;
using Entitas;
using UnityEngine;

public class InitializeVehicleSystem : ReactiveSystem<CoreEntity>
{
	readonly List<Node> _findCache;
	readonly HashSet<Node> _visitedCache;
	readonly List<int> _randomList;

	/// <inheritdoc />
	public InitializeVehicleSystem(IContext<CoreEntity> context) : base(context)
	{
		_findCache = new List<Node>(512);
		_visitedCache = new HashSet<Node>();
		_randomList = new List<int>(12);
	}

	/// <inheritdoc />
	protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
	{
		return context
			.GetGroup(CoreMatcher.AllOf(
				CoreMatcher.Vehicle,
				CoreMatcher.Team))
			.CreateCollector();
	}

	/// <inheritdoc />
	protected override bool Filter(CoreEntity entity)
	{
		return !entity.isAi
			&& !entity.hasVehicleSpawner
			&& entity.isEnabled;
	}

	/// <inheritdoc />
	protected override void Execute(List<CoreEntity> entities)
	{
		foreach (var entity in entities)
		{
			RandomCar(entity);

			// Cleanup
			_findCache.Clear();
			_visitedCache.Clear();

			// Determine spawn and destination
			var start = SetSpawnPoint(entity);
			var nodes = start.FindAll(
				x => x.Destination != null,
				_findCache,
				_visitedCache);

			// Get destination
			for (var i = 0;; i++)
			{
				var end = nodes.Random();
				if (i < 5 && end == start)
					continue;

				entity.AddDestination(end);
				break;
			}
			
			// Init transform
			entity.AddSteer(0);
			entity.AddSpeed(0);
			entity.AddThrottle(0);
			entity.AddPosition(start.Position);

			var delta = start.Nodes[0].Position - start.Position;
			entity.AddRotation(Quaternion.LookRotation(delta));
			entity.AddAngle(Mathf.Atan2(delta.x, delta.z));

			// Set some flags
			entity.isAi = true;
		}
	}

	public void RandomCar(CoreEntity entity)
	{
		_randomList.Clear();
		var vehicles = Game.Vehicles;
		var type = entity.vehicle.Type;
		for (var i = 0; i < vehicles.Length; i++)
		{
			if (vehicles[i].Vehicle.Type == type)
				_randomList.Add(i);
		}

		var available = _randomList.Random();
		var resource = vehicles[available];
		var vehicle = Object.Instantiate(resource);

		entity.AddAsset(
			vehicle.transform,
			vehicle.gameObject);

		entity.AddMotor(
			vehicle.Motor.Acceleration,
			vehicle.Motor.Deceleration,
			vehicle.Motor.MaxSpeed);

		if (type == VehicleType.Bus || type == VehicleType.Car)
		{
			entity.AddSize(
				vehicle.Size.Width,
				vehicle.Size.Height);
		}

		entity.AddSteerLock(
			vehicle.Steer.MaxAngle);
	}

	public Node SetSpawnPoint(CoreEntity entity)
	{
		if (entity.hasTarget)
			return entity.target.Target;
		
		_randomList.Clear();
		var nodes = Level.SpawnNodes;
		var type = entity.vehicle.Type;
		for (var i = 0; i < nodes.Length; i++)
		{
			if (HasType(type, nodes[i].Node))
				_randomList.Add(i);
		}
		
		var available = _randomList.Random();
		var node = nodes[available];

		entity.AddTarget(node?.Node);
		return entity.target.Target;
	}

	public static bool HasType(VehicleType type, Node node)
	{
		switch (type)
		{
			case VehicleType.Bike:
				return node.Bike != null;

			case VehicleType.Bus:
				return node.Bus != null;

			case VehicleType.Car:
				return node.Car != null;

			case VehicleType.Pedestrian:
				return node.Pedestrian != null;

			case VehicleType.Train:
				return node.Train != null;
		}

		return false;
	}
}
