﻿using Entitas;
using System.Collections.Generic;
using UnityEngine;

public class PolygonSystem : ReactiveSystem<CoreEntity>
{
	/// <inheritdoc />
	public PolygonSystem(IContext<CoreEntity> context) : base(context)
	{
		
	}

	/// <inheritdoc />
	protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
	{
		var group = context.GetGroup(
			CoreMatcher.AllOf(
				CoreMatcher.Position,
				CoreMatcher.Size,
				CoreMatcher.Angle));

		return group.CreateCollector();
	}

	/// <inheritdoc />
	protected override bool Filter(CoreEntity entity)
	{
		return entity.isEnabled;
	}

	/// <inheritdoc />
	protected override void Execute(List<CoreEntity> entities)
	{
		foreach (var entity in entities)
		{
			var verts = entity.hasPolygon
				? entity.polygon.Vertices
				: new Vector2[4];

			var angle = entity.angle.Angle;

			var size = entity.size;

			var position = new Vector2(
				entity.position.Value.x,
				entity.position.Value.z);

			var forward = new Vector2(
				Mathf.Cos(angle),
				Mathf.Sin(angle));

			var right = new Vector2(
				forward.y,
				forward.x);
			
			verts[0] = position + forward * size.Height + right * size.Width;
			verts[1] = position + forward * size.Height - right * size.Width;
			verts[2] = position + -forward * size.Height - right * size.Width;
			verts[3] = position + -forward * size.Height + right * size.Width;

			if (entity.hasPolygon)
				entity.ReplacePolygon(verts);
			else
				entity.AddPolygon(verts);
		}
	}
}

