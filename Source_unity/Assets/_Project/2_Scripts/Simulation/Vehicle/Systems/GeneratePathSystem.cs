﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class GeneratePathSystem : ReactiveSystem<CoreEntity>
{
	readonly List<Node> _cache;
	readonly List<Node> _path;


	/// <inheritdoc />
	public GeneratePathSystem(IContext<CoreEntity> context) : base(context)
	{
		_cache = new List<Node>(128);
		_path = new List<Node>(32);
	}

	/// <inheritdoc />
	protected override ICollector<CoreEntity> GetTrigger(
		IContext<CoreEntity> context)
	{
		return context.GetGroup(
				CoreMatcher.AllOf(
					CoreMatcher.Target,
					CoreMatcher.Destination))
			.CreateCollector(GroupEvent.Added);
	}

	/// <inheritdoc />
	protected override bool Filter(CoreEntity entity)
	{
		return !entity.hasPath
			&& entity.isEnabled;
	}

	/// <inheritdoc />
	protected override void Execute(List<CoreEntity> entities)
	{
		foreach (var entity in entities)
		{
			var start = entity.target.Target;
			var end = entity.destination.Destination;

			_path.Clear();
			_cache.Clear();
			SimplePath(start, end, _cache, _path);

			if (_path.Count == 0)
			{
				Debug.LogWarning($"Did not find path between {start} and {end}");
				continue;
			}

			var path = new Stack<Node>();
			for (var i = 0; i < _path.Count; i++)
				path.Push(_path[i]);
			
			entity.AddPath(path);
		}
	}

	static void SimplePath(
		Node current,
		Node target,
		List<Node> visited,
		ICollection<Node> path)
	{
		if (current == target)
		{
			path.Add(target);
			return;
		}

		if (visited.Contains(current))
			return;

		visited.Add(current);
		foreach (var node in current.Nodes)
		{
			SimplePath(node, target, visited, path);

			if (path.Count != 0)
			{
				path.Add(current);
				return;
			}
		}
	}
}
