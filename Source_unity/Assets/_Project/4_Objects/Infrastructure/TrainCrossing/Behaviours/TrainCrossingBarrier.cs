﻿using UnityEngine;

public class TrainCrossingBarrier : MonoBehaviour
{
	[SerializeField] Vector3 _upAngle;

	Quaternion _up;
	Quaternion _down;

	void Awake()
	{
		_down = transform.localRotation;
		_up = _down * Quaternion.Euler(_upAngle);
	}

	public void ApplyAlpha(float alpha)
	{
		transform.localRotation =
			Quaternion.SlerpUnclamped(_up, _down, alpha);
	}

	void OnDrawGizmosSelected()
	{
		var rotation = transform.rotation;

		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, rotation * Vector3.forward * 5);
		Gizmos.DrawRay(transform.position, rotation * Quaternion.Euler(_upAngle) * Vector3.forward * 5);
	}
}
