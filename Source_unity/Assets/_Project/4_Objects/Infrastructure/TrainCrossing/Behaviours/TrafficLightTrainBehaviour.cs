﻿using System;
using UnityEngine;

public class TrafficLightTrainBehaviour : TrafficLightBehaviour
{
	const float BlinkingSpeed = 0.3f;
	const float BarrierSpeed = 1.0f / 12.0f;

	[SerializeField] TrafficLightBulbRedBehaviour[] _lights;
	[SerializeField] AudioSource[] _audioSources;
	[SerializeField] TrainCrossingBarrier[] _barriers;
	[NonSerialized] public State Current;
	float _barrierAlpha;

	/// <inheritdoc />
	public override void SetState(int stateId)
	{
		switch (stateId)
		{
			case 0: // Stop
				if (Current == State.Idle)
					_barrierAlpha = -7.0f * BarrierSpeed;

				Current = State.Lowering;
				break;

			case 2: // Go
				if (Current == State.Lowering)
					Current = State.Raising;
				break;
		}
	}

	void Start()
	{
		ApplyBarrierAngle();
		Current = State.Idle;
	}

	void Update()
	{
		UpdateLights();
		UpdateAudio();

		UpdateBarriers();
		if (Current == State.Raising && _barrierAlpha < 0.001f)
			Current = State.Idle;
	}

	void UpdateBarriers()
	{
		if (Current == State.Idle)
			return;

		var delta = (Current == State.Lowering).ToMul()
			* BarrierSpeed
			* Game.CurrentEntity.timedelta.Delta;

		var newAlpha = delta > 0
			? Mathf.Min(1, _barrierAlpha + delta)
			: Mathf.Max(0, _barrierAlpha + delta);

		if (Mathf.Abs(_barrierAlpha - newAlpha) < 0.0001f)
			return;

		_barrierAlpha = newAlpha;
		ApplyBarrierAngle();
	}

	void ApplyBarrierAngle()
	{
		var alpha = Mathf.Clamp01(_barrierAlpha);
		foreach (var barrier in _barriers)
			barrier.ApplyAlpha(alpha);
	}

	void UpdateLights()
	{
		var on = Current != State.Idle;
		var even = Mathf.Repeat(Time.time, BlinkingSpeed) < BlinkingSpeed / 2;
		var uneven = !even;

		for (var i = 0; i < _lights.Length; i++)
			if (_lights[i] != null)
				_lights[i].IsOn = on && ((i & 1) == 1
					? even
					: uneven);
	}

	void UpdateAudio()
	{
		var on = Current != State.Idle;
		foreach (var audioSource in _audioSources)
		{
			if (on)
			{
				if (!audioSource.isPlaying)
					audioSource.Play();
			}
			else
			{
				if (audioSource.isPlaying)
					audioSource.Stop();
			}
		}
	}

	public enum State
	{
		Idle,
		Lowering,
		Raising,
	}
}
