﻿using UnityEngine;

public class TrafficLightBulbAmberBehaviour : TrafficLightBulbBehaviour
{
	[SerializeField] Texture2D _texture;

	/// <inheritdoc />
	public override Color Color => new Color(0.9f, 0.7f, 0.3f, 1.0f);

	/// <inheritdoc />
	public override Texture Texture => _texture;
}
