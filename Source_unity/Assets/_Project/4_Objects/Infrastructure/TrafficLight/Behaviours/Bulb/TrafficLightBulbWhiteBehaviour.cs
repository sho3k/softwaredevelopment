﻿using UnityEngine;

public class TrafficLightBulbWhiteBehaviour : TrafficLightBulbBehaviour
{
	/// <inheritdoc />
	public override Color Color => new Color(0.95f, 0.95f, 0.95f, 1.0f);

	/// <inheritdoc />
	public override Texture Texture => null;
}
