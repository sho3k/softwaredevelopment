﻿using UnityEngine;

public class TrafficLightBulbRedBehaviour : TrafficLightBulbBehaviour
{
	[SerializeField] Texture2D _texture;

	/// <inheritdoc />
	public override Color Color => new Color(0.9f, 0.2f, 0.1f, 1.0f);

	/// <inheritdoc />
	public override Texture Texture => _texture;
}
