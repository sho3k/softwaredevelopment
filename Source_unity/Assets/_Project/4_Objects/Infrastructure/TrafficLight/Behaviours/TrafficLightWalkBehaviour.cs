﻿using UnityEngine;

public class TrafficLightWalkBehaviour : TrafficLightBehaviour
{
	[SerializeField] TrafficLightBulbGreenBehaviour _green;
	[SerializeField] TrafficLightBulbRedBehaviour _red;

	/// <inheritdoc />
	public override void SetState(int stateId)
	{
		switch (stateId)
		{
			case 0:
				_green.IsOn = false;
				_red.IsOn = true;
				break;

			case 1:
				_green.IsOn = true;
				_red.IsOn = false;
				_green.StartBlink();
				break;

			case 2:
				_green.IsOn = true;
				_red.IsOn = false;
				break;
		}
	}
}
