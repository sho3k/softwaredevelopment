﻿using UnityEngine;

public class TrafficLightCarBehaviour : TrafficLightBehaviour
{
	[SerializeField] TrafficLightBulbGreenBehaviour _green;
	[SerializeField] TrafficLightBulbAmberBehaviour _amber;
	[SerializeField] TrafficLightBulbRedBehaviour _red;

	/// <inheritdoc />
	public override void SetState(int stateId)
	{
		switch (stateId)
		{
			case 0:
				_green.IsOn = false;
				_amber.IsOn = false;
				_red.IsOn = true;
				break;

			case 1:
				_green.IsOn = false;
				_amber.IsOn = true;
				_red.IsOn = false;
				break;

			case 2:
				_green.IsOn = true;
				_amber.IsOn = false;
				_red.IsOn = false;
				break;

			default:
				_green.IsOn = false;
				_amber.IsOn = true;
				_red.IsOn = false;
				_amber.StartBlink();
				break;
		}
	}
}
