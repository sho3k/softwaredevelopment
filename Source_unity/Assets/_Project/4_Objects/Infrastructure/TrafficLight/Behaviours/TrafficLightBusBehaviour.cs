﻿using System.Linq;
using UnityEngine;

public class TrafficLightBusBehaviour : TrafficLightBehaviour
{
	[Header("Row 0")]
	[SerializeField] TrafficLightBulbWhiteBehaviour _m00;
	[SerializeField] TrafficLightBulbWhiteBehaviour _m01;
	[SerializeField] TrafficLightBulbWhiteBehaviour _m02;

	[Header("Row 1")]
	[SerializeField] TrafficLightBulbRedBehaviour _m10;
	[SerializeField] TrafficLightBulbAmberBehaviour _m11;
	[SerializeField] TrafficLightBulbRedBehaviour _m12;

	[Header("Row 2")]
	[SerializeField] TrafficLightBulbWhiteBehaviour _m20;
	[SerializeField] TrafficLightBulbWhiteBehaviour _m21;
	[SerializeField] TrafficLightBulbWhiteBehaviour _m22;
	
	/// <inheritdoc />
	public override void SetState(int stateId)
	{
		switch (stateId)
		{
			case 0: // Stop
				SetLights(3, 5);
				break;
			case 1: // Stop if possible
				SetLights(4);
				break;
			case 2: // Straight
				SetLights(1, 7);
				break;
			case 3: // Left
				SetLights(0, 8);
				break;
			case 4: // Right
				SetLights(2, 6);
				break;
			case 5: // Left + Straight
				SetLights(0, 1, 7);
				break;
			case 6: // Right + Straight
				SetLights(1, 2, 7);
				break;
			case 7: // Left + Right
				SetLights(0, 2, 7);
				break;
			case 8: // All
				SetLights(0, 1, 2, 7);
				break;
		}
	}

	void SetLights(params int[] ids)
	{
		_m00.IsOn = ids.Contains(0);
		_m01.IsOn = ids.Contains(1);
		_m02.IsOn = ids.Contains(2);

		_m10.IsOn = ids.Contains(3);
		_m11.IsOn = ids.Contains(4);
		_m12.IsOn = ids.Contains(5);

		_m20.IsOn = ids.Contains(6);
		_m21.IsOn = ids.Contains(7);
		_m22.IsOn = ids.Contains(8);
	}
}
