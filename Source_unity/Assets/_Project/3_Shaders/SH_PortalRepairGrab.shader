﻿Shader "Custom/SH_PortalRepairGrab"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags
		{
			"RenderType"="Transparent"
			"Queue"="AlphaTest"
		}

		GrabPass
		{
			"_BackTex"
		}
		
		Pass
		{
			Cull Back
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f
			{
				float4 grabPos : TEXCOORD0;
				float4 pos : SV_POSITION;
			};

			v2f vert(appdata_full v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.grabPos = ComputeGrabScreenPos(o.pos);
				return o;
			}

			sampler2D _BackTex;

			float4 frag(v2f i) : SV_Target
			{
				float4 back = tex2Dproj(_BackTex, i.grabPos);
				return back;
			}
			ENDCG
		}
		
	}
}
