﻿Shader "Custom/SH_Portal"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader
	{
		Tags
		{
			"RenderType"="Transparent"
			"Queue"="Transparent+1"
		}

		Pass
		{
			ColorMask 0
			ZWrite On

			Stencil
			{
				Ref 1
				Comp Less
				Pass Replace
				WriteMask 1
			}
		}

		Pass
		{
			Cull Back
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f
			{
				float4 grabPos : TEXCOORD0;
				float4 pos : SV_POSITION;
			};

			v2f vert(appdata_full v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.grabPos = ComputeGrabScreenPos(o.pos);
				return o;
			}

			sampler2D _BackTex;

			float4 frag(v2f i) : SV_Target
			{
				float4 back = tex2Dproj(_BackTex, i.grabPos);
				return back;
			}
			ENDCG
		}
	}

	FallBack "Diffuse"
}
