﻿Shader "Custom/SH_PortalRepair"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_DepthParams("Depth", Float) = 0.09
	}
	SubShader
	{
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha

		Tags
		{
			"RenderType" = "Transparent"
			"Queue" = "Transparent"
		}

		CGPROGRAM
		#include "UnityCG.cginc" 
		#pragma surface surf StandardSpecular vertex:vert alpha:fade 
		#ifdef SHADER_API_D3D11 
		#pragma target 4.0 
		#else 
		#pragma target 3.0 
		#endif 

		sampler2D _MainTex;
		sampler2D _CameraDepthTexture;

		float4 _Color;
		float _DepthParams;

		struct Input
		{
			float2 uv_MainTex;
			float4 screenPos;
			float eyeDepth;
		};

		void vert(inout appdata_full v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
			COMPUTE_EYEDEPTH(o.eyeDepth);
		}

		void surf(Input i, inout SurfaceOutputStandardSpecular o)
		{
			float depth = SAMPLE_DEPTH_TEXTURE_PROJ(
				_CameraDepthTexture,
				UNITY_PROJ_COORD(i.screenPos));

			depth = LinearEyeDepth(depth);
			depth -= i.eyeDepth;
			depth *= _DepthParams;
			depth = saturate(1 - depth) * 0.99;

			o.Albedo = _Color.rgb;
			o.Alpha = depth * _Color.a;
		}
		ENDCG

		
	}
	FallBack "Diffuse"
}
