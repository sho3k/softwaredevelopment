﻿Shader "Custom/SH_VehicleMask"
{
	SubShader
	{
		Tags
		{
			"RenderType" = "Transparent"
			"Queue" = "Geometry-100"
		}

		Pass
		{
			ZWrite On
			ColorMask 0
			Stencil
			{
				Ref 2
				Comp Always
				Pass Replace
				WriteMask 2
			}
		}
	}

	FallBack "Diffuse"
}
