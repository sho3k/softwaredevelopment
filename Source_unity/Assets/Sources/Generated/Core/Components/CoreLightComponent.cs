//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class CoreEntity {

    public LightComponent light { get { return (LightComponent)GetComponent(CoreComponentsLookup.Light); } }
    public bool hasLight { get { return HasComponent(CoreComponentsLookup.Light); } }

    public void AddLight(TrafficLightBehaviour[] newLights) {
        var index = CoreComponentsLookup.Light;
        var component = CreateComponent<LightComponent>(index);
        component.Lights = newLights;
        AddComponent(index, component);
    }

    public void ReplaceLight(TrafficLightBehaviour[] newLights) {
        var index = CoreComponentsLookup.Light;
        var component = CreateComponent<LightComponent>(index);
        component.Lights = newLights;
        ReplaceComponent(index, component);
    }

    public void RemoveLight() {
        RemoveComponent(CoreComponentsLookup.Light);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class CoreMatcher {

    static Entitas.IMatcher<CoreEntity> _matcherLight;

    public static Entitas.IMatcher<CoreEntity> Light {
        get {
            if (_matcherLight == null) {
                var matcher = (Entitas.Matcher<CoreEntity>)Entitas.Matcher<CoreEntity>.AllOf(CoreComponentsLookup.Light);
                matcher.componentNames = CoreComponentsLookup.componentNames;
                _matcherLight = matcher;
            }

            return _matcherLight;
        }
    }
}
