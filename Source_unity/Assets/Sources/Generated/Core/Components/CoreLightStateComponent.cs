//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class CoreEntity {

    public LightStateComponent lightState { get { return (LightStateComponent)GetComponent(CoreComponentsLookup.LightState); } }
    public bool hasLightState { get { return HasComponent(CoreComponentsLookup.LightState); } }

    public void AddLightState(int newId, int newStatus, float newTime) {
        var index = CoreComponentsLookup.LightState;
        var component = CreateComponent<LightStateComponent>(index);
        component.Id = newId;
        component.Status = newStatus;
        component.Time = newTime;
        AddComponent(index, component);
    }

    public void ReplaceLightState(int newId, int newStatus, float newTime) {
        var index = CoreComponentsLookup.LightState;
        var component = CreateComponent<LightStateComponent>(index);
        component.Id = newId;
        component.Status = newStatus;
        component.Time = newTime;
        ReplaceComponent(index, component);
    }

    public void RemoveLightState() {
        RemoveComponent(CoreComponentsLookup.LightState);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class CoreMatcher {

    static Entitas.IMatcher<CoreEntity> _matcherLightState;

    public static Entitas.IMatcher<CoreEntity> LightState {
        get {
            if (_matcherLightState == null) {
                var matcher = (Entitas.Matcher<CoreEntity>)Entitas.Matcher<CoreEntity>.AllOf(CoreComponentsLookup.LightState);
                matcher.componentNames = CoreComponentsLookup.componentNames;
                _matcherLightState = matcher;
            }

            return _matcherLightState;
        }
    }
}
