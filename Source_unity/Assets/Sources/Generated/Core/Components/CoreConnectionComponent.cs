//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class CoreEntity {

    public ConnectionComponent connection { get { return (ConnectionComponent)GetComponent(CoreComponentsLookup.Connection); } }
    public bool hasConnection { get { return HasComponent(CoreComponentsLookup.Connection); } }

    public void AddConnection(Connection newConnection, Dispatcher<Response> newDispatcher) {
        var index = CoreComponentsLookup.Connection;
        var component = CreateComponent<ConnectionComponent>(index);
        component.Connection = newConnection;
        component.Dispatcher = newDispatcher;
        AddComponent(index, component);
    }

    public void ReplaceConnection(Connection newConnection, Dispatcher<Response> newDispatcher) {
        var index = CoreComponentsLookup.Connection;
        var component = CreateComponent<ConnectionComponent>(index);
        component.Connection = newConnection;
        component.Dispatcher = newDispatcher;
        ReplaceComponent(index, component);
    }

    public void RemoveConnection() {
        RemoveComponent(CoreComponentsLookup.Connection);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class CoreMatcher {

    static Entitas.IMatcher<CoreEntity> _matcherConnection;

    public static Entitas.IMatcher<CoreEntity> Connection {
        get {
            if (_matcherConnection == null) {
                var matcher = (Entitas.Matcher<CoreEntity>)Entitas.Matcher<CoreEntity>.AllOf(CoreComponentsLookup.Connection);
                matcher.componentNames = CoreComponentsLookup.componentNames;
                _matcherConnection = matcher;
            }

            return _matcherConnection;
        }
    }
}
