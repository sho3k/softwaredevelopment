//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class CoreEntity {

    public TimedeltaComponent timedelta { get { return (TimedeltaComponent)GetComponent(CoreComponentsLookup.Timedelta); } }
    public bool hasTimedelta { get { return HasComponent(CoreComponentsLookup.Timedelta); } }

    public void AddTimedelta(float newDelta) {
        var index = CoreComponentsLookup.Timedelta;
        var component = CreateComponent<TimedeltaComponent>(index);
        component.Delta = newDelta;
        AddComponent(index, component);
    }

    public void ReplaceTimedelta(float newDelta) {
        var index = CoreComponentsLookup.Timedelta;
        var component = CreateComponent<TimedeltaComponent>(index);
        component.Delta = newDelta;
        ReplaceComponent(index, component);
    }

    public void RemoveTimedelta() {
        RemoveComponent(CoreComponentsLookup.Timedelta);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class CoreMatcher {

    static Entitas.IMatcher<CoreEntity> _matcherTimedelta;

    public static Entitas.IMatcher<CoreEntity> Timedelta {
        get {
            if (_matcherTimedelta == null) {
                var matcher = (Entitas.Matcher<CoreEntity>)Entitas.Matcher<CoreEntity>.AllOf(CoreComponentsLookup.Timedelta);
                matcher.componentNames = CoreComponentsLookup.componentNames;
                _matcherTimedelta = matcher;
            }

            return _matcherTimedelta;
        }
    }
}
