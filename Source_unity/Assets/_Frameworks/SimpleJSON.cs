﻿#if !UNITY_WEBPLAYER
#define USE_FileIO
#endif
/* * * * *
 * A simple JSON Parser / builder
 * ------------------------------
 * 
 * It mainly has been written as a simple JSON parser. It can build a JSON string
 * from the node-tree, or generate a node tree from any valid JSON string.
 * 
 * If you want to use compression when saving to file / stream / B64 you have to include
 * SharpZipLib ( http://www.icsharpcode.net/opensource/sharpziplib/ ) in your project and
 * define "USE_SharpZipLib" at the top of the file
 * 
 * Written by Bunny83 
 * 2012-06-09
 * 
 *
 * Features / attributes:
 * - provides strongly typed node classes and lists / dictionaries
 * - provides easy access to class members / array items / data values
 * - the parser now properly identifies types. So generating JSON with this framework should work.
 * - only double quotes (") are used for quoting strings.
 * - provides "casting" properties to easily convert to / from those types:
 *   int / float / double / bool
 * - provides a common interface for each node so no explicit casting is required.
 * - the parser tries to avoid errors, but if malformed JSON is parsed the result is more or less undefined
 * - It can serialize/deserialize a node tree into/from an experimental compact binary format. It might
 *   be handy if you want to store things in a file and don't want it to be easily modifiable
 * 
 * 
 * 2012-12-17 Update:
 * - Added internal JSONLazyCreator class which simplifies the construction of a JSON tree
 *   Now you can simple reference any item that doesn't exist yet and it will return a JSONLazyCreator
 *   The class determines the required type by it's further use, creates the type and removes itself.
 * - Added binary serialization / deserialization.
 * - Added support for BZip2 zipped binary format. Requires the SharpZipLib ( http://www.icsharpcode.net/opensource/sharpziplib/ )
 *   The usage of the SharpZipLib library can be disabled by removing or commenting out the USE_SharpZipLib define at the top
 * - The serializer uses different types when it comes to store the values. Since my data values
 *   are all of type string, the serializer will "try" which format fits best. The order is: int, float, double, bool, string.
 *   It's not the most efficient way but for a moderate amount of data it should work on all platforms.
 * 
 * 2017-03-08 Update:
 * - Optimised parsing by using a StringBuilder for token. This prevents performance issues when large
 *   string data fields are contained in the json data.
 * - Finally refactored the badly named JSONClass into JSONObject.
 * - Replaced the old JSONData class by distict typed classes ( JSONString, JSONNumber, JSONBool, JSONNull ) this
 *   allows to propertly convert the node tree back to json without type information loss. The actual value
 *   parsing now happens at parsing time and not when you actually access one of the casting properties.
 * 
 * 2017-04-11 Update:
 * - Fixed parsing bug where empty string values have been ignored.
 * - Optimised "ToString" by using a StringBuilder internally. This should heavily improve performance for large files
 * - Changed the overload of "ToString(string aIndent)" to "ToString(int aIndent)"
 * 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2012-2017 Markus Göbel
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * * * * */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace SimpleJSON
{
	public enum JSONNodeType
	{
		Array = 1,
		Object = 2,
		String = 3,
		Number = 4,
		NullValue = 5,
		Boolean = 6,
		None = 7,
	}
	
	public abstract class JSONNode
	{
		public static string FixKey(string key)
		{
			var builder = new StringBuilder();

			foreach (var c in key)
			{
				if (char.IsLetter(c))
					builder.Append(c);
			}
			
			return builder.ToString();
		}

		#region common interface
		public virtual JSONNode this[int aIndex]
		{
			get { return null; }
			set { }
		}

		public virtual JSONNode this[string aKey]
		{
			get { return null; }
			set { }
		}

		public virtual string Value
		{
			get { return ""; }
			set { }
		}

		public virtual int Count => 0;

		public virtual bool IsNumber => false;
		public virtual bool IsString => false;
		public virtual bool IsBoolean => false;
		public virtual bool IsNull => false;
		public virtual bool IsArray => false;
		public virtual bool IsObject => false;

		public virtual void Add(string aKey, JSONNode aItem)
		{
		}

		public virtual void Add(JSONNode aItem)
		{
			Add("", aItem);
		}

		public virtual JSONNode Remove(string aKey)
		{
			return null;
		}

		public virtual JSONNode Remove(int aIndex)
		{
			return null;
		}

		public virtual JSONNode Remove(JSONNode aNode)
		{
			return aNode;
		}

		public virtual IEnumerable<JSONNode> Children
		{
			get { yield break; }
		}

		public IEnumerable<JSONNode> DeepChildren
		{
			get
			{
				foreach (var C in Children)
				foreach (var D in C.DeepChildren)
					yield return D;
			}
		}

		public override string ToString()
		{
			var sb = new StringBuilder();
			WriteToStringBuilder(sb);
			return sb.ToString();
		}
		
		internal abstract void WriteToStringBuilder(StringBuilder aSB);
		#endregion common interface


		#region typecasting properties
		public abstract JSONNodeType Tag { get; }

		public virtual double AsDouble
		{
			get
			{
				double v;
				if (double.TryParse(Value, out v))
					return v;

				return 0.0;
			}
			set { Value = value.ToString(); }
		}

		public virtual int AsInt
		{
			get { return (int)AsDouble; }
			set { AsDouble = value; }
		}

		public virtual float AsFloat
		{
			get { return (float)AsDouble; }
			set { AsDouble = value; }
		}

		public virtual bool AsBool
		{
			get
			{
				bool v;
				if (bool.TryParse(Value, out v))
					return v;

				return !string.IsNullOrEmpty(Value);
			}
			set
			{
				Value = value
					? "true"
					: "false";
			}
		}

		public virtual JSONArray AsArray => this as JSONArray;

		public virtual JSONObject AsObject => this as JSONObject;
		#endregion typecasting properties


		#region operators
		public static implicit operator JSONNode(string s)
		{
			return new JSONString(s);
		}

		public static implicit operator string(JSONNode d)
		{
			return d == null
				? null
				: d.Value;
		}

		public static implicit operator JSONNode(double n)
		{
			return new JSONNumber(n);
		}

		public static implicit operator double(JSONNode d)
		{
			return d == null
				? 0
				: d.AsDouble;
		}

		public static implicit operator JSONNode(float n)
		{
			return new JSONNumber(n);
		}

		public static implicit operator float(JSONNode d)
		{
			return d == null
				? 0
				: d.AsFloat;
		}

		public static implicit operator JSONNode(int n)
		{
			return new JSONNumber(n);
		}

		public static implicit operator int(JSONNode d)
		{
			return d == null
				? 0
				: d.AsInt;
		}

		public static implicit operator JSONNode(bool b)
		{
			return new JSONBool(b);
		}

		public static implicit operator bool(JSONNode d)
		{
			return d != null && d.AsBool;
		}

		public static bool operator ==(JSONNode a, object b)
		{
			if (ReferenceEquals(a, b))
				return true;

			var aIsNull = a is JSONNull || ReferenceEquals(a, null)
				|| a is JSONLazyCreator;
			var bIsNull = b is JSONNull || ReferenceEquals(b, null)
				|| b is JSONLazyCreator;
			if (aIsNull && bIsNull)
				return true;

			return a.Equals(b);
		}

		public static bool operator !=(JSONNode a, object b)
		{
			return !(a == b);
		}

		public override bool Equals(object obj)
		{
			return ReferenceEquals(this, obj);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
		#endregion operators


		internal static StringBuilder m_EscapeBuilder = new StringBuilder();

		internal static string Escape(string aText)
		{
			m_EscapeBuilder.Length = 0;
			if (m_EscapeBuilder.Capacity < aText.Length + aText.Length / 10)
				m_EscapeBuilder.Capacity = aText.Length + aText.Length / 10;
			foreach (var c in aText)
			{
				switch (c)
				{
					case '\\':
						m_EscapeBuilder.Append("\\\\");
						break;
					case '\"':
						m_EscapeBuilder.Append("\\\"");
						break;
					case '\'':
						m_EscapeBuilder.Append("\\'");
						break;
					case '\n':
						m_EscapeBuilder.Append("\\n");
						break;
					case '\r':
						m_EscapeBuilder.Append("\\r");
						break;
					case '\t':
						m_EscapeBuilder.Append("\\t");
						break;
					case '\b':
						m_EscapeBuilder.Append("\\b");
						break;
					case '\f':
						m_EscapeBuilder.Append("\\f");
						break;
					default:
						m_EscapeBuilder.Append(c);
						break;
				}
			}

			var result = m_EscapeBuilder.ToString();
			m_EscapeBuilder.Length = 0;
			return result;
		}

		static void ParseElement(JSONNode ctx, string token, string tokenName,
			bool quoted)
		{
			if (quoted)
			{
				ctx.Add(tokenName, token);
				return;
			}

			var tmp = token.ToLower();
			if (tmp == "false" || tmp == "true")
				ctx.Add(tokenName, tmp == "true");
			else if (tmp == "null")
				ctx.Add(tokenName, null);
			else
			{
				double val;
				if (double.TryParse(token, out val))
					ctx.Add(tokenName, val);
				else
					ctx.Add(tokenName, token);
			}
		}

		public static JSONNode Parse(string aJSON)
		{
			var stack = new Stack<JSONNode>();
			JSONNode ctx = null;
			var i = 0;
			var Token = new StringBuilder();
			var TokenName = "";
			var QuoteMode = false;
			var TokenIsQuoted = false;
			while (i < aJSON.Length)
			{
				switch (aJSON[i])
				{
					case '{':
						if (QuoteMode)
						{
							Token.Append(aJSON[i]);
							break;
						}

						stack.Push(new JSONObject());
						if (ctx != null)
							ctx.Add(TokenName, stack.Peek());
						TokenName = "";
						Token.Length = 0;
						ctx = stack.Peek();
						break;

					case '[':
						if (QuoteMode)
						{
							Token.Append(aJSON[i]);
							break;
						}

						stack.Push(new JSONArray());
						if (ctx != null)
							ctx.Add(TokenName, stack.Peek());
						TokenName = "";
						Token.Length = 0;
						ctx = stack.Peek();
						break;

					case '}':
					case ']':
						if (QuoteMode)
						{
							Token.Append(aJSON[i]);
							break;
						}

						if (stack.Count == 0)
							throw new Exception("JSON Parse: Too many closing brackets");

						stack.Pop();
						if (Token.Length > 0 || TokenIsQuoted)
						{
							ParseElement(ctx, Token.ToString(), TokenName, TokenIsQuoted);
							TokenIsQuoted = false;
						}
						TokenName = "";
						Token.Length = 0;
						if (stack.Count > 0)
							ctx = stack.Peek();
						break;

					case ':':
						if (QuoteMode)
						{
							Token.Append(aJSON[i]);
							break;
						}

						TokenName = Token.ToString();
						Token.Length = 0;
						TokenIsQuoted = false;
						break;

					case '"':
						QuoteMode ^= true;
						TokenIsQuoted |= QuoteMode;
						break;

					case ',':
						if (QuoteMode)
						{
							Token.Append(aJSON[i]);
							break;
						}

						if (Token.Length > 0 || TokenIsQuoted)
						{
							ParseElement(ctx, Token.ToString(), TokenName, TokenIsQuoted);
							TokenIsQuoted = false;
						}
						TokenName = "";
						Token.Length = 0;
						TokenIsQuoted = false;
						break;

					case '\r':
					case '\n':
						break;

					case ' ':
					case '\t':
						if (QuoteMode)
							Token.Append(aJSON[i]);
						break;

					case '\\':
						++i;
						if (QuoteMode)
						{
							var C = aJSON[i];
							switch (C)
							{
								case 't':
									Token.Append('\t');
									break;
								case 'r':
									Token.Append('\r');
									break;
								case 'n':
									Token.Append('\n');
									break;
								case 'b':
									Token.Append('\b');
									break;
								case 'f':
									Token.Append('\f');
									break;
								case 'u':
								{
									var s = aJSON.Substring(i + 1, 4);
									Token.Append(
										(char)int.Parse(
											s,
											NumberStyles.AllowHexSpecifier));
									i += 4;
									break;
								}
								default:
									Token.Append(C);
									break;
							}
						}

						break;

					default:
						Token.Append(aJSON[i]);
						break;
				}

				++i;
			}

			if (QuoteMode)
				throw new Exception("JSON Parse: Quotation marks seems to be messed up.");

			return ctx;
		}

		public virtual void Serialize(BinaryWriter aWriter)
		{
		}

		public void SaveToStream(Stream aData)
		{
			var W = new BinaryWriter(aData);
			Serialize(W);
		}

#if USE_SharpZipLib
		public void SaveToCompressedStream(System.IO.Stream aData)
		{
			using (var gzipOut =
new ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream(aData))
			{
				gzipOut.IsStreamOwner = false;
				SaveToStream(gzipOut);
				gzipOut.Close();
			}
		}
 
		public void SaveToCompressedFile(string aFileName)
		{
 
#if USE_FileIO
			System.IO.Directory.CreateDirectory((new System.IO.FileInfo(aFileName)).Directory.FullName);
			using(var F = System.IO.File.OpenWrite(aFileName))
			{
				SaveToCompressedStream(F);
			}
 
#else
			throw new Exception("Can't use File IO stuff in the webplayer");
#endif
		}
		public string SaveToCompressedBase64()
		{
			using (var stream = new System.IO.MemoryStream())
			{
				SaveToCompressedStream(stream);
				stream.Position = 0;
				return System.Convert.ToBase64String(stream.ToArray());
			}
		}
 
#else
		public void SaveToCompressedStream(Stream aData)
		{
			throw new Exception(
				"Can't use compressed functions. You need include the SharpZipLib and uncomment the define at the top of SimpleJSON");
		}

		public void SaveToCompressedFile(string aFileName)
		{
			throw new Exception(
				"Can't use compressed functions. You need include the SharpZipLib and uncomment the define at the top of SimpleJSON");
		}

		public string SaveToCompressedBase64()
		{
			throw new Exception(
				"Can't use compressed functions. You need include the SharpZipLib and uncomment the define at the top of SimpleJSON");
		}
#endif

		public void SaveToFile(string aFileName)
		{
#if USE_FileIO
			Directory.CreateDirectory(new FileInfo(aFileName).Directory.FullName);
			using (var F = File.OpenWrite(aFileName))
				SaveToStream(F);
#else
			throw new Exception ("Can't use File IO stuff in the webplayer");
#endif
		}

		public string SaveToBase64()
		{
			using (var stream = new MemoryStream())
			{
				SaveToStream(stream);
				stream.Position = 0;
				return Convert.ToBase64String(stream.ToArray());
			}
		}

		public static JSONNode Deserialize(BinaryReader aReader)
		{
			var type = (JSONNodeType)aReader.ReadByte();
			switch (type)
			{
				case JSONNodeType.Array:
				{
					var count = aReader.ReadInt32();
					var tmp = new JSONArray();
					for (var i = 0; i < count; i++)
						tmp.Add(Deserialize(aReader));

					return tmp;
				}
				case JSONNodeType.Object:
				{
					var count = aReader.ReadInt32();
					var tmp = new JSONObject();
					for (var i = 0; i < count; i++)
					{
						var key = aReader.ReadString();
						var val = Deserialize(aReader);
						tmp.Add(key, val);
					}

					return tmp;
				}
				case JSONNodeType.String:
				{
					return new JSONString(aReader.ReadString());
				}
				case JSONNodeType.Number:
				{
					return new JSONNumber(aReader.ReadDouble());
				}
				case JSONNodeType.Boolean:
				{
					return new JSONBool(aReader.ReadBoolean());
				}
				case JSONNodeType.NullValue:
				{
					return new JSONNull();
				}
				default:
				{
					throw new Exception("Error deserializing JSON. Unknown tag: " + type);
				}
			}
		}

#if USE_SharpZipLib
		public static JSONNode LoadFromCompressedStream(System.IO.Stream aData)
		{
			var zin = new ICSharpCode.SharpZipLib.BZip2.BZip2InputStream(aData);
			return LoadFromStream(zin);
		}
		public static JSONNode LoadFromCompressedFile(string aFileName)
		{
#if USE_FileIO
			using(var F = System.IO.File.OpenRead(aFileName))
			{
				return LoadFromCompressedStream(F);
			}
#else
			throw new Exception("Can't use File IO stuff in the webplayer");
#endif
		}
		public static JSONNode LoadFromCompressedBase64(string aBase64)
		{
			var tmp = System.Convert.FromBase64String(aBase64);
			var stream = new System.IO.MemoryStream(tmp);
			stream.Position = 0;
			return LoadFromCompressedStream(stream);
		}
#else
		public static JSONNode LoadFromCompressedFile(string aFileName)
		{
			throw new Exception(
				"Can't use compressed functions. You need include the SharpZipLib and uncomment the define at the top of SimpleJSON");
		}

		public static JSONNode LoadFromCompressedStream(Stream aData)
		{
			throw new Exception(
				"Can't use compressed functions. You need include the SharpZipLib and uncomment the define at the top of SimpleJSON");
		}

		public static JSONNode LoadFromCompressedBase64(string aBase64)
		{
			throw new Exception(
				"Can't use compressed functions. You need include the SharpZipLib and uncomment the define at the top of SimpleJSON");
		}
#endif

		public static JSONNode LoadFromStream(Stream aData)
		{
			using (var R = new BinaryReader(aData))
				return Deserialize(R);
		}

		public static JSONNode LoadFromFile(string aFileName)
		{
#if USE_FileIO
			using (var F = File.OpenRead(aFileName))
				return LoadFromStream(F);
#else
			throw new Exception ("Can't use File IO stuff in the webplayer");
#endif
		}

		public static JSONNode LoadFromBase64(string aBase64)
		{
			var tmp = Convert.FromBase64String(aBase64);
			var stream = new MemoryStream(tmp);
			stream.Position = 0;
			return LoadFromStream(stream);
		}
	}
	
	public class JSONArray : JSONNode, IEnumerable
	{
		readonly List<JSONNode> _list = new List<JSONNode>();

		public override JSONNodeType Tag => JSONNodeType.Array;
		public override bool IsArray => true;

		public override JSONNode this[int aIndex]
		{
			get
			{
				if (aIndex < 0 || aIndex >= _list.Count)
					return new JSONLazyCreator(this);

				return _list[aIndex];
			}
			set
			{
				if (value == null)
					value = new JSONNull();
				if (aIndex < 0 || aIndex >= _list.Count)
					_list.Add(value);
				else
					_list[aIndex] = value;
			}
		}

		public override JSONNode this[string aKey]
		{
			get { return new JSONLazyCreator(this); }
			set
			{
				if (value == null)
					value = new JSONNull();
				_list.Add(value);
			}
		}

		public override int Count => _list.Count;

		public override IEnumerable<JSONNode> Children
		{
			get
			{
				foreach (var N in _list)
					yield return N;
			}
		}

		public IEnumerator GetEnumerator()
		{
			foreach (var N in _list)
				yield return N;
		}

		public void Clear()
		{
			_list.Clear();
		}

		public override void Add(string aKey, JSONNode aItem)
		{
			if (aItem == null)
				aItem = new JSONNull();
			_list.Add(aItem);
		}

		public override JSONNode Remove(int aIndex)
		{
			if (aIndex < 0 || aIndex >= _list.Count)
				return null;

			var tmp = _list[aIndex];
			_list.RemoveAt(aIndex);
			return tmp;
		}

		public override JSONNode Remove(JSONNode aNode)
		{
			_list.Remove(aNode);
			return aNode;
		}

		public override void Serialize(BinaryWriter aWriter)
		{
			aWriter.Write((byte)JSONNodeType.Array);
			aWriter.Write(_list.Count);
			for (var i = 0; i < _list.Count; i++)
				_list[i].Serialize(aWriter);
		}

		internal override void WriteToStringBuilder(StringBuilder aSB)
		{
			aSB.Append('[');
			var count = _list.Count;

			for (var i = 0; i < count; i++)
			{
				if (i > 0) aSB.Append(',');
				_list[i].WriteToStringBuilder(aSB);
			}
			
			aSB.Append(']');
		}
	}

	// End of JSONArray

	public class JSONObject : JSONNode, IEnumerable
	{
		public Dictionary<string, JSONNode> Dictionary = new Dictionary<string, JSONNode>();

		public override JSONNodeType Tag => JSONNodeType.Object;
		public override bool IsObject => true;


		public override JSONNode this[string aKey]
		{
			get
			{
				aKey = FixKey(aKey);
				return Dictionary.ContainsKey(aKey)
					? Dictionary[aKey]
					: null;
			}
			set
			{
				aKey = FixKey(aKey);
				if (value == null)
					value = new JSONNull();

				if (Dictionary.ContainsKey(aKey))
					Dictionary[aKey] = value;
				else
					Dictionary.Add(aKey, value);
			}
		}

		public override JSONNode this[int aIndex]
		{
			get
			{
				if (aIndex < 0 || aIndex >= Dictionary.Count)
					return null;

				return Dictionary.ElementAt(aIndex).Value;
			}
			set
			{
				if (value == null)
					value = new JSONNull();
				if (aIndex < 0 || aIndex >= Dictionary.Count)
					return;

				var key = Dictionary.ElementAt(aIndex).Key;
				Dictionary[key] = value;
			}
		}

		public override int Count => Dictionary.Count;

		public override IEnumerable<JSONNode> Children
		{
			get
			{
				foreach (var N in Dictionary)
					yield return N.Value;
			}
		}

		public IEnumerator GetEnumerator()
		{
			foreach (var N in Dictionary)
				yield return N;
		}

		public override void Add(string aKey, JSONNode aItem)
		{
			if (aItem == null)
				aItem = new JSONNull();

			if (!string.IsNullOrEmpty(aKey))
			{
				aKey = FixKey(aKey);
				if (Dictionary.ContainsKey(aKey))
					Dictionary[aKey] = aItem;
				else
					Dictionary.Add(aKey, aItem);
			}
			else
				Dictionary.Add(Guid.NewGuid().ToString(), aItem);
		}

		public override JSONNode Remove(string aKey)
		{
			aKey = FixKey(aKey);
			if (!Dictionary.ContainsKey(aKey))
				return null;

			var tmp = Dictionary[aKey];
			Dictionary.Remove(aKey);
			return tmp;
		}

		public override JSONNode Remove(int aIndex)
		{
			if (aIndex < 0 || aIndex >= Dictionary.Count)
				return null;

			var item = Dictionary.ElementAt(aIndex);
			Dictionary.Remove(item.Key);
			return item.Value;
		}

		public override JSONNode Remove(JSONNode aNode)
		{
			try
			{
				var item = Dictionary.First(k => k.Value == aNode);
				Dictionary.Remove(item.Key);
				return aNode;
			}
			catch
			{
				return null;
			}
		}

		public override void Serialize(BinaryWriter aWriter)
		{
			aWriter.Write((byte)JSONNodeType.Object);
			aWriter.Write(Dictionary.Count);
			foreach (var K in Dictionary.Keys)
			{
				aWriter.Write(K);
				Dictionary[K].Serialize(aWriter);
			}
		}

		internal override void WriteToStringBuilder(StringBuilder aSB)
		{
			aSB.Append('{');
			var first = true;

			foreach (var k in Dictionary)
			{
				if (!first)
					aSB.Append(',');

				first = false;
				aSB.Append('\"')
					.Append(Escape(k.Key))
					.Append('\"')
					.Append(':');

				k.Value.WriteToStringBuilder(aSB);
			}
			
			aSB.Append('}');
		}
	}

	// End of JSONObject

	public class JSONString : JSONNode
	{
		string m_Data;

		public JSONString(string aData)
		{
			m_Data = aData;
		}

		public override JSONNodeType Tag => JSONNodeType.String;
		public override bool IsString => true;

		public override string Value
		{
			get { return m_Data; }
			set { m_Data = value; }
		}

		public override void Serialize(BinaryWriter aWriter)
		{
			aWriter.Write((byte)JSONNodeType.String);
			aWriter.Write(m_Data);
		}

		internal override void WriteToStringBuilder(StringBuilder aSB)
		{
			aSB.Append('\"').Append(Escape(m_Data)).Append('\"');
		}

		public override bool Equals(object obj)
		{
			if (base.Equals(obj))
				return true;

			var s = obj as string;
			if (s != null)
				return m_Data == s;

			var s2 = obj as JSONString;
			if (s2 != null)
				return m_Data == s2.m_Data;

			return false;
		}

		public override int GetHashCode()
		{
			return m_Data.GetHashCode();
		}
	}

	// End of JSONString

	public sealed class JSONNumber : JSONNode, IDisposable
	{
		static readonly Stack<JSONNumber> _pool = new Stack<JSONNumber>();

		public static JSONNumber CreateInstance(double value)
		{
			if (_pool.Count == 0)
				return new JSONNumber(value);

			var item = _pool.Pop();
			item.Data = value;
			return item;
		}
		
		public double Data;

		public JSONNumber(double aData)
		{
			Data = aData;
		}

		public JSONNumber(string aData)
		{
			Value = aData;
		}

		public override JSONNodeType Tag => JSONNodeType.Number;
		public override bool IsNumber => true;


		public override string Value
		{
			get { return Data.ToString(CultureInfo.InvariantCulture); }
			set
			{
				double v;
				if (double.TryParse(value, out v))
					Data = v;
			}
		}

		public override double AsDouble
		{
			get { return Data; }
			set { Data = value; }
		}

		public override void Serialize(BinaryWriter aWriter)
		{
			aWriter.Write((byte)JSONNodeType.Number);
			aWriter.Write(Data);
		}

		internal override void WriteToStringBuilder(StringBuilder aSB)
		{
			aSB.Append(Data);
		}

		static bool IsNumeric(object value)
		{
			return value is int || value is uint
				|| value is float || value is double
				|| value is decimal
				|| value is long || value is ulong
				|| value is short || value is ushort
				|| value is sbyte || value is byte;
		}

		public override bool Equals(object obj)
		{
			if (obj == null)
				return false;
			if (base.Equals(obj))
				return true;

			var s2 = obj as JSONNumber;
			if (s2 != null)
				return Data == s2.Data;
			if (IsNumeric(obj))
				return Convert.ToDouble(obj) == Data;

			return false;
		}

		public override int GetHashCode()
		{
			return Data.GetHashCode();
		}

		/// <inheritdoc />
		public void Dispose()
		{
			_pool.Push(this);
		}
	}

	// End of JSONNumber

	public class JSONBool : JSONNode
	{
		bool m_Data;

		public JSONBool(bool aData)
		{
			m_Data = aData;
		}

		public JSONBool(string aData)
		{
			Value = aData;
		}

		public override JSONNodeType Tag => JSONNodeType.Boolean;
		public override bool IsBoolean => true;


		public override string Value
		{
			get { return m_Data.ToString(); }
			set
			{
				bool v;
				if (bool.TryParse(value, out v))
					m_Data = v;
			}
		}
		public override bool AsBool
		{
			get { return m_Data; }
			set { m_Data = value; }
		}

		public override void Serialize(BinaryWriter aWriter)
		{
			aWriter.Write((byte)JSONNodeType.Boolean);
			aWriter.Write(m_Data);
		}

		internal override void WriteToStringBuilder(StringBuilder aSB)
		{
			aSB.Append(
				m_Data
					? "true"
					: "false");
		}

		public override bool Equals(object obj)
		{
			if (obj == null)
				return false;
			if (obj is bool)
				return m_Data == (bool)obj;

			return false;
		}

		public override int GetHashCode()
		{
			return m_Data.GetHashCode();
		}
	}

	// End of JSONBool

	public class JSONNull : JSONNode
	{
		public override JSONNodeType Tag => JSONNodeType.NullValue;
		public override bool IsNull => true;

		public override string Value
		{
			get { return "null"; }
			set { }
		}
		public override bool AsBool
		{
			get { return false; }
			set { }
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
				return true;

			return obj is JSONNull;
		}

		public override int GetHashCode()
		{
			return 0;
		}

		public override void Serialize(BinaryWriter aWriter)
		{
			aWriter.Write((byte)JSONNodeType.NullValue);
		}

		internal override void WriteToStringBuilder(StringBuilder aSB)
		{
			aSB.Append("null");
		}
	}

	// End of JSONNull

	class JSONLazyCreator : JSONNode
	{
		JSONNode m_Node = null;
		string m_Key = null;

		public JSONLazyCreator(JSONNode aNode)
		{
			m_Node = aNode;
			m_Key = null;
		}

		public JSONLazyCreator(JSONNode aNode, string aKey)
		{
			m_Node = aNode;
			m_Key = aKey;
		}

		public override JSONNodeType Tag => JSONNodeType.None;

		public override JSONNode this[int aIndex]
		{
			get { return new JSONLazyCreator(this); }
			set
			{
				var tmp = new JSONArray();
				tmp.Add(value);
				Set(tmp);
			}
		}

		public override JSONNode this[string aKey]
		{
			get { return new JSONLazyCreator(this, aKey); }
			set
			{
				var tmp = new JSONObject();
				tmp.Add(aKey, value);
				Set(tmp);
			}
		}

		public override int AsInt
		{
			get
			{
				var tmp = new JSONNumber(0);
				Set(tmp);
				return 0;
			}
			set
			{
				var tmp = new JSONNumber(value);
				Set(tmp);
			}
		}

		public override float AsFloat
		{
			get
			{
				var tmp = new JSONNumber(0.0f);
				Set(tmp);
				return 0.0f;
			}
			set
			{
				var tmp = new JSONNumber(value);
				Set(tmp);
			}
		}

		public override double AsDouble
		{
			get
			{
				var tmp = new JSONNumber(0.0);
				Set(tmp);
				return 0.0;
			}
			set
			{
				var tmp = new JSONNumber(value);
				Set(tmp);
			}
		}

		public override bool AsBool
		{
			get
			{
				var tmp = new JSONBool(false);
				Set(tmp);
				return false;
			}
			set
			{
				var tmp = new JSONBool(value);
				Set(tmp);
			}
		}

		public override JSONArray AsArray
		{
			get
			{
				var tmp = new JSONArray();
				Set(tmp);
				return tmp;
			}
		}

		public override JSONObject AsObject
		{
			get
			{
				var tmp = new JSONObject();
				Set(tmp);
				return tmp;
			}
		}

		void Set(JSONNode aVal)
		{
			if (m_Key == null)
				m_Node.Add(aVal);
			else
				m_Node.Add(m_Key, aVal);
			m_Node = null; // Be GC friendly.
		}

		public override void Add(JSONNode aItem)
		{
			var tmp = new JSONArray();
			tmp.Add(aItem);
			Set(tmp);
		}

		public override void Add(string aKey, JSONNode aItem)
		{
			var tmp = new JSONObject();
			tmp.Add(aKey, aItem);
			Set(tmp);
		}

		public static bool operator ==(JSONLazyCreator a, object b)
		{
			if (b == null)
				return true;

			return ReferenceEquals(a, b);
		}

		public static bool operator !=(JSONLazyCreator a, object b)
		{
			return !(a == b);
		}

		public override bool Equals(object obj)
		{
			if (obj == null)
				return true;

			return ReferenceEquals(this, obj);
		}

		public override int GetHashCode()
		{
			return 0;
		}

		internal override void WriteToStringBuilder(StringBuilder aSB)
		{
			aSB.Append("null");
		}
	}

	// End of JSONLazyCreator

	public static class JSON
	{
		public static JSONNode Parse(string aJSON)
		{
			return JSONNode.Parse(aJSON);
		}
	}
}
