﻿Shader "Custom/DebugLine"
{
	Properties
	{
		[PerRenderData] _Color("Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			Cull Off

			BindChannels
			{
				Bind "vertex", vertex Bind "color", color
			}
		}
	}
}
