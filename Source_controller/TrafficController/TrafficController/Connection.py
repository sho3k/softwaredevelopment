import threading
import pika
import json
from collections import namedtuple
from Intersection import Intersection

NAME = "groep"
TEAM = "/2"
CONTROLLER_QUEUE_NAME = "controller"
SIMULATOR_QUEUE_NAME = "simulator"
EXCHANGE = ""

HOST = "localhost"
PORT = 5672


class Connection:
	"""Connection handler"""
	def __init__(self, intersection):
		self.__connection = pika.BlockingConnection(
			pika.ConnectionParameters(
				virtual_host=TEAM,
				host=HOST,
				port=PORT,
				credentials=pika.PlainCredentials(
					username="guest",
					password="guest")))

		self.__channel = self.__connection.channel()
		self.__channel.queue_declare(
			queue=CONTROLLER_QUEUE_NAME,
			auto_delete=True,
			arguments={'x-message-ttl' : 10000})
		thread = threading.Thread(target=self.__start)
		thread.start()
		thread.join(0)
		
		self.intersection = intersection


	def _json_object_hook(self, d): 
		return namedtuple('X', d.keys())(*d.values())
	
	def json2obj(self, data): 
		return json.loads(data, object_hook=self._json_object_hook)

	def __start(self):
		self.__channel.basic_consume(
			consumer_callback=self.__consume,
			queue=CONTROLLER_QUEUE_NAME);

		self.__channel.start_consuming();
		print("Connection started")

	# callback
	def __consume(self, channel, method, properties, body):

		# Get Content
		content = body.decode("utf-8", "ignore")

		# Change json to python obj
		received = self.json2obj(content)
		
        # Process updates into the objects
		self.intersection.handleUpdate(received)
    
    # Send a message to the simulator
	def emit(self, message):
		self.__channel.basic_publish(
			exchange=EXCHANGE,
			routing_key=SIMULATOR_QUEUE_NAME,
			body=str(message))

	def close(self):
		self.__channel.close()
		self.__connection.close()