# Class containing all Lights methods.
class Lights:
	def __init__(self):
		LightsIDS = [101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 201, 301, 302, 303, 304, 305, 401, 402, 403, 404, 405, 406, 501, 601]
		self.__LightsDict = {
			"Lights": []
		}

		for id in LightsIDS:	
			if id == 601:
				dict = {"Id":id, "Status":2, "Time": -1}
			else:
				dict = {"Id":id, "Status":0, "Time": -1}
			self.__LightsDict["Lights"].append(dict)


	
	#Check Light status
	def __checkStatus(self, LightId):
		for x in self.__LightsDict["Lights"]:	
				if x["Id"] == LightId:
					return x["Status"]
	#Make Light Green
	def __Go(self, LightId):
		for x in self.__LightsDict["Lights"]:
			if x["Id"] == LightId:
				x["Status"] = 2
				print(self.__LightsDict)
				return self.__LightsDict;
				
	#Make Light Red
	def __Stop(self, LightId):
		for x in self.__LightsDict["Lights"]:
			if x["Id"] == LightId:
				x["Status"] = 0
				print(self.__LightsDict)
				return self.__LightsDict;
				
	def changeStatus(self, x):
		LightId = x.TrafficUpdate.LightId
		if x.TrafficUpdate.Count > 0:
			for x in self.__LightsDict["Lights"]:	
				if x['Id'] == LightId:
					if x['Id'] > 600:
						x["Status"] = 0
					else:
						x["Status"] = 2
			return self.__LightsDict;
			print(self.__LightsDict)				
		else:
			for x in self.__LightsDict["Lights"]:
				if x['Id'] == LightId:
					if x['Id'] > 600:
						x["Status"] = 2
					else:
						x["Status"] = 0
			return self.__LightsDict;
			print(self.__LightsDict)
	
	def changeStat(self, x):
		
		l101 = self.__checkStatus(101)
		l102 = self.__checkStatus(102)
		l103 = self.__checkStatus(103)
		l104 = self.__checkStatus(104)
		l105 = self.__checkStatus(105)
		l106 = self.__checkStatus(106)
		l107 = self.__checkStatus(107)
		l108 = self.__checkStatus(108)
		l109 = self.__checkStatus(109)
		l110 = self.__checkStatus(110)
		l201 = self.__checkStatus(201)
		l301 = self.__checkStatus(301)
		l302 = self.__checkStatus(302)
		l303 = self.__checkStatus(303)
		l304 = self.__checkStatus(304)
		l305 = self.__checkStatus(305)
		l401 = self.__checkStatus(401)
		l402 = self.__checkStatus(402)
		l403 = self.__checkStatus(403)
		l404 = self.__checkStatus(404)
		l405 = self.__checkStatus(405)
		l406 = self.__checkStatus(406)
		l501 = self.__checkStatus(501)
		l601 = self.__checkStatus(601)
		LightId = x.TrafficUpdate.LightId

		if x.TrafficUpdate.Count > 0:
			for x in self.__LightsDict["Lights"]:	
				if LightId == 101:
					if l105 > 0 or l107 > 0 or l201 > 0 or l301 > 0 or l302 > 0 or l303 > 0 or l305 > 0 or l401 > 0 or l402 > 0 or l403 > 0 or l406 > 0:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 102:
					if l105 > 0 or l107 > 0 or l108 > 0 or l109 > 0 or l110 > 0 or l201 > 0 or l302 > 0 or l303 > 0 or l402 > 0 or l403 > 0 or l501 > 0 or l601 < 2:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 103:
					if l105 > 0 or l106 > 0 or l109 > 0 or l110 > 0 or l201 > 0 or l302 > 0 or l303 > 0 or l304 > 0 or l402 > 0 or l403 > 0 or l404 > 0 or l405 > 0:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 104:
					if l110 > 0 or l201 > 0 or l302 > 0 or l303 > 0 or l304 > 0 or l402 > 0 or l403 > 0 or l404 > 0 or l405 > 0:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 105:
					if l101 > 0 or l102 > 0 or l103 > 0 or l106 > 0 or l107 > 0 or l110 > 0 or l201 > 0 or l301 > 0 or l304 > 0 or l305 > 0 or l401 > 0 or l404 > 0 or l405 > 0 or l406 > 0:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 106:
					if l103 > 0 or l104 > 0 or l105 > 0 or l109 > 0 or l110 > 0 or l201 > 0 or l302 > 0 or l303 > 0 or l304 > 0 or l402 > 0 or l403 > 0 or l404 > 0 or l405 > 0 or l501 > 0 or l601 < 2:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 107:
					if l101 > 0 or l102 > 0 or l105 > 0 or l109 > 0 or l110 > 0 or l201 > 0 or l301 > 0 or l305 > 0 or l401 > 0 or l406 > 0 or l501 > 0 or l601 < 2:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 108:
					if l102 > 0 or l301 > 0 or l305 > 0 or l401 > 0 or l406 > 0 or l501 > 0 or l601 < 2:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 109:
					if l102 > 0 or l103 > 0 or l106 > 0 or l107 > 0 or l301 > 0 or l304 > 0 or l305 > 0 or l401 > 0 or l404 > 0 or l405 > 0 or l406 > 0:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 110:
					if l102 > 0 or l103 > 0 or l104 > 0 or l105 > 0 or l106 > 0 or l107 > 0 or l201 > 0 or l302 > 0 or l303 > 0 or l304 > 0 or l402 > 0 or l403 > 0 or l404 > 0 or l405 > 0 or l501 > 0 or l601 < 2:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 201:
					if l101 > 0 or l102 > 0 or l103 > 0 or l104 > 0 or l105 > 0 or l106 > 0 or l107 > 0 or l110 > 0 or l302 > 0 or l301 > 0 or l302 > 0 or l303 > 0 or l304 > 0 or l305 > 0 or l401 > 0 or l402 > 0 or l403 > 0 or l404 > 0 or l405 > 0 or l406 > 0:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 301:
					if l101 > 0 or l105 > 0 or l107 > 0 or l108 > 0 or l109 > 0 or l110 > 0 or l201 > 0:
						self.__Stop(LightId)
					else:
						return self.__Go(LightId);
				elif LightId == 302:
					if l101 > 0 or l102 > 0 or l103 > 0 or l104 > 0 or l106 > 0 or l110 > 0 or l201 > 0:
						self.__Stop(LightId)
					else:
						return self.__Go(LightId);
				elif LightId == 303:
					if l101 > 0 or l102 > 0 or l103 > 0 or l104 > 0 or l106 > 0 or l110 > 0 or l201 > 0:
						self.__Stop(LightId)
					else:
						return self.__Go(LightId);
				elif LightId == 304:
					if l103 > 0 or l104 > 0 or l105 > 0 or l106 > 0 or l109 > 0 or l201 > 0:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 305:
					if l101 > 0 or l105 > 0 or l107 > 0 or l108 > 0 or l109 > 0 or l110 > 0 or l201 > 0:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 401:
					if l101 > 0 or l105 > 0 or l107 > 0 or l108 > 0 or l109 > 0 or l110 > 0 or l201 > 0:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 402:
					if l101 > 0 or l102 > 0 or l103 > 0 or l104 > 0 or l106 > 0 or l110 > 0 or l201 > 0:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 403:
					if l101 > 0 or l102 > 0 or l103 > 0 or l104 > 0 or l106 > 0 or l110 > 0 or l201 > 0:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 404:
					if l103 > 0 or l104 > 0 or l105 > 0 or l106 > 0 or l109 > 0 or l201 > 0:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 405:
					if l103 > 0 or l104 > 0 or l105 > 0 or l106 > 0 or l109 > 0 or l201 > 0:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 406:
					if l101 > 0 or l105 > 0 or l107 > 0 or l108 > 0 or l109 > 0 or l110 > 0 or l201 > 0:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 501:
					if l102 > 0 or l106 > 0 or l107 > 0 or l108 > 0:
						return self.__Stop(LightId);
					else:
						return self.__Go(LightId);
				elif LightId == 601:
					if l102 > 0 or l106 > 0 or l107 > 0 or l108 > 0:
						return self.__Go(LightId);
					else:
						return self.__Stop(LightId);
		else:
			return self.__Stop(LightId);

