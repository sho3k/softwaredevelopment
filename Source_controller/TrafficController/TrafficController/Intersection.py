from Light import *

class Intersection(object):
    """Main intersection manager"""
    priorityList = []
    
    #Lights that aren't compatible.
    CHECKS = { 101:[105, 107, 201, 301, 302, 303, 401, 402, 403, 406],
                102: [105, 107, 108, 109, 110, 201, 302, 303, 402, 407, 601],
                103: [105, 106, 107, 109, 110, 201, 302, 303, 304, 402, 407, 405, 410],
                104: [106, 110, 201, 302, 303, 304, 408, 403, 409, 404],
                105: [101, 102, 103, 106, 107, 110, 201, 301, 304, 305, 404, 409, 401, 412],
                106: [103, 104, 105, 109, 110, 201, 302, 303, 402, 403, 501, 601],
                107: [101, 102, 103, 105, 109, 110, 201, 301, 305, 401, 412, 601],
                108: [102, 301, 305, 411, 406, 601],
                109: [102, 103, 106, 107, 301, 304, 305, 411, 410, 405, 406],
                110: [102, 103, 104, 105, 106, 107, 201, 301, 302, 303, 305, 411, 408, 403, 406],
                201: [101, 102, 103, 104, 105, 106, 107, 110, 301, 302, 303, 304, 305, 401, 402, 403, 404, 405, 406],
                301: [101, 105, 107, 108, 109, 110, 201],
                302: [101, 102, 103, 104, 106, 109, 110, 201],
                303: [101, 102, 103, 104, 106, 109, 110, 201],
                304: [103, 104, 105, 106, 109, 201],
                305: [101, 105, 107, 108, 109, 110, 201],
                401: [101, 105, 107, 201],
                402: [101, 102, 103],
                403: [104, 106, 110, 201],
                404: [201, 104, 105],
                405: [103, 106, 109],
                406: [108, 109, 110],
                407: [101, 102, 103],
                408: [104, 106, 110, 201],
                409: [201, 104, 105],
                410: [103, 106, 109],
                411: [108, 109, 110],
                412: [101, 105, 107, 201],
                501: [102, 108, 106, 107],
                502: [102, 108, 106, 107],
                601: []
               }

    #Lights not compatible with bus directions.
    BUSCHECKS = { 1: [],
                 2: [101, 102, 103, 106, 107, 110, 201, 301, 304, 305, 404, 409, 401, 412],
                 3: [102, 103, 104, 105, 110, 109, 108, 107, 106, 404, 409, 303, 304],
                 4: [106, 110, 201, 302, 303, 304, 408, 403, 409, 404],
                 5: [],
                 6: [],
                 7: [],
                 8: []
                 }

    def __init__(self):
        self.lights = {101:TrafficLight(101, self), 102:TrafficLight(102, self), 103:TrafficLight(103, self), 104:TrafficLight(104, self), 105:TrafficLight(105, self), 106:TrafficLight(106, self), 107:TrafficLight(107, self), 108:TrafficLight(108, self), 109:TrafficLight(109, self), 110:TrafficLight(110, self), 201:BusLight(201, self), 301:BikeLight(301, self), 302:BikeLight(302, self), 303:BikeLight(303, self), 304:BikeLight(304, self), 305:BikeLight(305, self), 401:PedestrianLight(401, self), 402:PedestrianLight(402, self), 403:PedestrianLight(403, self), 404:PedestrianLight(404, self), 405:PedestrianLight(405, self), 406:PedestrianLight(406, self), 407:PedestrianLight(407, self), 408:PedestrianLight(408, self), 409:PedestrianLight(409, self), 410:TrafficLight(410, self), 411:TrafficLight(411, self), 412:TrafficLight(412, self), 501:TrainLight(501, self), 502:TrainLight(502, self), 601:RailroadCrossingLight(601, self)}
        self.lastClear = time.time()

    def getLightById(self, Id):
        return self.lights.get(Id)

    def handleUpdate(self, jsonObject):
        light = self.lights.get(jsonObject.TrafficUpdate.LightId)
        light.handleUpdate(jsonObject)

    # Convert the lightobjects to the correct json format.		
    def toJson(self):
		
        lightsDict = {
            "Lights": []
        }

        for key in self.lights:
            if key != 502:
                light = self.lights.get(key)
                value = 0
                colors = [1,2,3,4,5,6,7,8]
                for color in colors: # WTF is dit
                    if light.Status == color:
                        value = color
                    
                dict = {"Id":light.Id, "Status":value, "Time": light.Time}
                lightsDict["Lights"].append(dict)

        return lightsDict

    def timeInSeconds(self, val):
        if val == 0:
            return 0
        else:
            return time.time() - val

    # Check if a light can go green
    def allClear(self, Id):
        compat = self.CHECKS.get(Id)
        for value in compat:
            light = self.getLightById(value)
            if value == 601:
                if light.getStatus() != 2:
                    return False
            else:
                # light is not red and light has not been green/yellow for long enough (3 seconds)
                if light.Status != 0 or self.timeInSeconds(light.redTime) < 6:
                    return False
        return True

    #Check if the buspath is clear.
    def busClear(self, direction):
        directionconf = self.BUSCHECKS.get(direction)
        for id in directionconf:
            light = self.getLightById(id)
            if light.Status > LightColor.RED:
                return False
        return True

    #Check for conflicts in the busroute
    def busConflict(self, direction, lightId):

        if lightId in self.BUSCHECKS.get(direction):
            return False
        else:
            return True
    
    # Create a  list with all the Lights, sorted by priority
    def lightsByPriority(self):
        
        lightsList = list(self.lights.values())
        lightsList.sort(key=lambda x: x.getPriority(), reverse=True)
        
        return lightsList

    
    def debugLights(self):
        message = ''
        for light in self.lights.values():
            message += str(light.Id) + ': ' + str(light.Status) + ' '

    # Forceclear if a certain lane is waiting too long
    def forceClear(self, Id):
        compatible = self.CHECKS.get(Id)
        timeSinceClear = self.timeInSeconds(self.lastClear)
        if timeSinceClear > 12:
            print("In forceclear")
            self.lastClear = time.time()
            for value in compatible:
                light = self.getLightById(value)
                if light.Id < 600:
                    if light.Status != 0:
                        light.setRed()

    # Same as forceclear, used for train           
    def hardClear(self, Id):
        compatible = self.CHECKS.get(Id)
        timeSinceClear = self.timeInSeconds(self.lastClear)
        print("In HARDclear")
        self.lastClear = time.time()
        for value in compatible:
            light = self.getLightById(value)
            if light.Status != 0:
                light.setRed()

    # Train incoming
    def trainComing(self, Id):
        railRoadCrossingId = 601

        railRoadCrossing = self.lights.get(railRoadCrossingId)
        self.hardClear(railRoadCrossingId)

        railRoadCrossing.setRed()
        print("Train Coming")

        light = self.lights.get(Id)
        sensor = self.lights.get(502)
        if light.Id == 501:
            if sensor.Count > 0:
                light.setRed()
                print("Set light green (Sensor)")
                sensor.setGreen()
            else:
                x = Timer(16, light.setGreen)
                x.start()
                sensor.setRed()
        else:
            sensor.setGreen()
    
    # Train Leaving
    def trainLeaving(self, Id):
        railRoadCrossingId = 601
        railRoadCrossing = self.lights.get(railRoadCrossingId)
        railRoadCrossing.setGreen()

        light = self.lights.get(Id)
        light.setRed();
        print("Train LEAVING")

            



       