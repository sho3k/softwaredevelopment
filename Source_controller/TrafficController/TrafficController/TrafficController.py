from tkinter import *
from Connection import Connection
from Intersection import Intersection
from Light import LightColor
from Light import Light
from Light import TrainLight
from Light import RailroadCrossingLight
from Light import BusLight
from Light import TrafficLight
from Light import BikeLight
from Light import PedestrianLight
import time
import os

# Create classe
intersection = Intersection()
connection = Connection(intersection)

clear = lambda: os.system('cls')

cycle = 0

while True:
    clear()
    pTime = time.time()
    print("TrafficController - KruispuntSimulatie")
    """Stoplichten op rood zetten"""
    for lightsId in intersection.lights:
        light = intersection.lights.get(lightsId)
        
        if isinstance(light, TrainLight):
            #Treinlogica
            if light.Count == 0 and light.Status != LightColor.RED and light.Status != LightColor.YELLOW:
                intersection.trainLeaving(light.Id)
        elif isinstance(light, RailroadCrossingLight):
            p = 1
        elif isinstance(light, BusLight):
            #Buslogica
            if light.Count == 0 and light.Status != LightColor.RED and light.Status != LightColor.YELLOW: #Geen autos, niet rood, niet oranje.
                light.setRed()
        elif isinstance(light, PedestrianLight) or isinstance(light, BikeLight):
            #Als er geen voertuigen meer staan en licht staat nog op groen
            if light.Count == 0 and light.Status == LightColor.GREEN: 
                light.setRed()
            elif intersection.timeInSeconds(light.greenTime) > 10:
                light.setRed()
        else:
            #Als er geen voertuigen meer staan en licht staat nog op groen
            if light.Count == 0 and light.Status == LightColor.GREEN: 
                light.setRed()
            elif intersection.timeInSeconds(light.greenTime) > 15:
                light.setRed()

       
        

    """Stoplichten op groen Zetten"""
    for light in intersection.lightsByPriority():

        if light.getPriority() > 1 and light.Status == LightColor.RED:
            print(str(light.Id) + ": " + str(light.getPriority()))

        if isinstance(light, TrainLight): #Treinlogica
            if light.Count > 0:
                intersection.trainComing(light.Id)
        elif isinstance(light, RailroadCrossingLight):  #Railroadcrossing logic
            p = 0
        elif isinstance(light, BusLight): #BUSSES           
            if light.Count > 0 and light.Status != light.DirectionRequests[0]: #Bus waiting and current status != to direction
                if intersection.busClear(light.DirectionRequests[0]):
                    light.Status = light.DirectionRequests[0]

        elif isinstance(light, PedestrianLight) or isinstance(light, BikeLight):  #PEDESTRIANS + BIKERS
            if intersection.allClear(light.Id):
                if light.Count > 0 and light.Status == LightColor.RED:
                    if intersection.timeInSeconds(light.redTime) > 10:
                        busLight = intersection.lights.get(201)
                        if busLight.Count > 0 and intersection.busConflict(busLight.DirectionRequests[0], light.Id) and busLight.redTime > 15:
                            print("Ja bus heeft voorrang")   
                        else:
                            light.setGreen()
            elif light.getPriority() > 25:
                intersection.forceClear(light.Id)
        else:
            if intersection.allClear(light.Id):
                if light.Count > 0 and light.Status == LightColor.RED:
                    if intersection.timeInSeconds(light.redTime) > 10:
                        busLight = intersection.lights.get(201)
                        if busLight.Count > 0 and intersection.busConflict(busLight.DirectionRequests[0], light.Id) and busLight.redTime > 15:
                            print("Ja bus heeft voorrang")   
                        else:

                            print(light.Id)
                            light.setGreen()
            elif light.getPriority() > 25:
                intersection.forceClear(light.Id)

    connection.emit(intersection.toJson())



    cycle = cycle + 1
    timeDiff = time.time() - pTime
    timeOverhead = 1 - timeDiff
    if timeOverhead < 0:
        timeOverhead = 0
    time.sleep(timeOverhead)


