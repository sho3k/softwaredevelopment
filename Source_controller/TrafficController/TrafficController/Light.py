import time
import Intersection
from Light import *
from threading import Timer
from enum import IntEnum

class LightColor(IntEnum):
    GREEN = 2
    YELLOW = 1
    RED = 0

    def value(self):
        return self.value

class BusLightColor(IntEnum):
    RED = 0
    YELLOW = 1
    FORWARD = 2
    LEFT = 3
    RIGHT = 4
    LEFT_FORWARD = LEFT + FORWARD
    RIGHT_FORWARD = RIGHT + FORWARD
    LEFT_RIGHT = LEFT + RIGHT
    ALL = 8

    def value(self):
        return self.value

class Light(object):
    """Main Light Class."""
    
    def __init__(self, Id, parent):
        self.Id = Id
        self.Status = LightColor.RED
        self.Time = -1
        self.Count = 0
        self.DirectionRequests = []
        self.lastRequest = 0
        self.greenTime = 0
        self.redTime = time.time()
        self.timeWeight = 0.5
        self.countWeight = 1
        self.intersection = parent
    
    # Process incoming updates to the light objects
    def handleUpdate(self, jsonObject):
        previousCount = self.Count
        self.Count = jsonObject.TrafficUpdate.Count
        self.DirectionRequests = jsonObject.TrafficUpdate.DirectionRequests
		
        if previousCount == 0 and self.Count > 0: # A vehicle has arrived and this light wants to go green 
            self.lastRequest = time.time() # Set the time of this event happening
        elif previousCount > 0 and self.Count > 0 and self.lastRequest == 0: # A vehicle has arrived and this light wants to go green
            self.lastRequest = time.time()
    
    # Return the priority of a light (Calculated by amount of cars waiting and time)
    def getPriority(self):
        combo = self.timeWeight * self.timeInSeconds(self.lastRequest)
        combo2 = self.countWeight + self.Count
        return combo2 + combo
    
    # Checks if light can go green with the current running lights.
    def checkCompatibility(self, Id):
        if Id in CHECKS.get(Id):
            self.CHECKS.get
    
    def getStatus(self):
        return self.Status

    # Convert time.time() to seconds for easier use.
    def timeInSeconds(self, val):
        if val == 0:
            return 0
        else:
            return time.time() - val
    
    # Set light RED
    def orangeRed(self):
        self.lastRequest = 0
        self.Status = LightColor.RED
    
    # Change light to Orange and start a timer for Red.
    def setRed(self):
        self.Status = LightColor.YELLOW
        t = Timer(2, self.orangeRed)
        t.start()
        self.greenTime = 0
        self.redTime = time.time()

    # Set light Green
    def setGreen(self):
        self.Status = LightColor.GREEN
        self.redTime = 0
        self.greenTime = time.time()

class TrafficLight(Light): # Default trafficlight (Cars)

    def __init__(self, Id, parent):
        Light.__init__(self, Id, parent)
        self.timeWeight = 0.5

class PedestrianLight(Light):

    def __init__(self, Id, parent):
        Light.__init__(self, Id, parent)
        self.timeWeight = 0.2


class BikeLight(Light): 

    def __init__(self, Id, parent):
        Light.__init__(self, Id, parent)
        self.timeWeight = 0.2

class BusLight(Light):
    ROOD = 0
    ORANGE = 1
    RECHTDOOR = 2
    LINKSAF = 3
    RECHTSAF = 4
    LINKSRECHTDOOR = 5
    RECHTSRECHTDOOR = 6
    LINKSAFRECHTSAF = 7
    ALLE = 8
    
    def __init__(self, Id, parent):
        Light.__init__(self, Id, parent)

    def setGreen(self):
        self.Status = 3
        self.redTime = 0
        self.greenTime = time.time()

class TrainLight(Light):
    def __init__(self, Id, parent):
        Light.__init__(self, Id, parent)
        self.timeWeight = 30
        self.countWeight = 50

class RailroadCrossingLight(Light):
    def __init__(self, Id, parent):
        Light.__init__(self, Id, parent)
        self.Status = 2

    # Railroadcrossing going down  
    def setRed(self):
        self.Status = LightColor.RED
        self.greenTime = 0
        print("Railroadcrossing going down")

    # Railroadcrossing going up
    def setGreen(self):
        t = Timer(5, self.delayGreen)
        t.start()

    def delayGreen(self):
        self.Status = LightColor.GREEN
        self.redTime = 0
        self.greenTime = time.time()


